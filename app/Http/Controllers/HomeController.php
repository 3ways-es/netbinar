<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Evento;
use App\EventosEquipo;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * 3 Ways - Carlos Colmenarez
     * Mostrar página home con elementos "publicados"
     */
    public function index(Evento $evento=null, $preview=null) {

        $preview = (substr((app('Illuminate\Routing\UrlGenerator')->full()),-7)) ? true : false;

        
        $evento_actual = (setting('site.evento_actual') !== null) ? setting('site.evento_actual') : Evento::first()->id;
        $evento = $evento ?? Evento::find($evento_actual);
        
        //Si no está registrado ningún evento llevar a la vista de mantenimiento
        if(!$evento) return view('mantenimiento');

        $eventos = Evento::publicados()->whereNotIn('id', [$evento->id])->orderBy('orden')->get();
        $categorias = Categoria::where('evento_id', $evento->id)->publicados()->orderBy('order')->get();
        $equipo = EventosEquipo::where('evento_id', $evento->id)->orderBy('order')->get();

        return (setting('site.modo_mantenimiento') && !Auth::check()) ? view('mantenimiento') : view('home', compact('categorias', 'evento', 'eventos', 'equipo'));
    }
    /**
     * Mostrar preview de la página (todos los elementos publicados y no publicados)
     */
    public function preview($evento=null) {

        $evento_actual = (setting('site.evento_actual') !== null) ? setting('site.evento_actual') : Evento::first()->id;
        $evento = $evento ?? Evento::find($evento_actual);

        $eventos = Evento::whereNotIn('id', [$evento->id])->orderBy('orden')->get();
        $categorias = Categoria::where('evento_id', $evento->id)->orderBy('order')->get();
        $equipo = EventosEquipo::where('evento_id', $evento->id)->orderBy('order')->get();

        return (setting('site.modo_mantenimiento') && !Auth::check()) ? view('mantenimiento') : view('home', compact('categorias', 'evento', 'eventos', 'equipo'));
    }
}