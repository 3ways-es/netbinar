<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use Illuminate\Support\Facades\Auth;

class VideoController extends Controller
{
    public function index($id) {
        $video = Video::findOrFail($id);

        if(!$video->publicado){
            if(Auth::check()){
                $videos = Video::whereDate('fecha', '=',\Carbon\Carbon::parse($video->fecha)->format('Y/m/d'))->whereNotIn('id', [$id])->get();
                return view('video', compact('video','videos'));
            }else{
                abort(404);
            }
        }else{
            $videos = Video::whereDate('fecha', '=',\Carbon\Carbon::parse($video->fecha)->format('Y/m/d'))->whereNotIn('id', [$id])->get();
            return view('video', compact('video','videos'));
        }
    }
}
