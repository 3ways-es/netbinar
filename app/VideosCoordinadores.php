<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Carlos Colmenarez
 * Modelo pivot "videos_coordinadores"
 */

class VideosCoordinadores extends Model
{
    public $with = ['participante'];

    public function participante() {
       return $this->belongsTo(Participante::class);
    }
}
