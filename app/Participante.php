<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Carlos Colmenarez
 * Modelo "participantes"
 */

class Participante extends Model
{
    public $additional_attributes = ['nombre_completo'];
    public $appends = ['nombre_completo'];

    public function getNombreCompletoAttribute() {
        return "{$this->tratamiento} {$this->nombre} {$this->apellidos}";
    }

    public function eventos_equipo() {
        return $this->hasMany(EventosEquipo::class);
    }
}
