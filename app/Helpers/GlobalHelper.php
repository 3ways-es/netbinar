<?php
//Funciones globales para toda la web
function colorHexToRGB($hex) {
    $hex = str_replace("#","",$hex);
    $split = str_split($hex, 2);
    $r = hexdec($split[0]);
    $g = hexdec($split[1]);
    $b = hexdec($split[2]);
    return $r . ", " . $g . ", " . $b;
}