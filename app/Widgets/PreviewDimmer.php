<?php

namespace App\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;

class PreviewDimmer extends \TCG\Voyager\Widgets\BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $string = 'Preview';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-search',
            'title'  => "{$string}",
            'text'   => 'Ver la Preview de la Web',
            'button' => [
                    'text' => 'Ver Preview',
                    'link' => route('preview'),
                    'class' => 'primary',
                ],
            'image' => '',
        ]));
    }
}
