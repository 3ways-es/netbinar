<?php

namespace App\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use App\Evento;

class EventosDimmer extends \TCG\Voyager\Widgets\BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $string = 'Eventos';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-trophy',
            'title'  => "{$string}",
            'text'   => 'Gestión los Eventos',
            'button' => [
                    'text' => 'Ver los Eventos',
                    'link' => route('voyager.eventos.index'),
                    'class' => 'primary',
                ],
            'image' => '',
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', app(Evento::class));
    }
}
