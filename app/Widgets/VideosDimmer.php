<?php

namespace App\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use App\Video;

class VideosDimmer extends \TCG\Voyager\Widgets\BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $string = 'Videos';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-youtube-play',
            'title'  => "{$string}",
            'text'   => 'Gestión los videos',
            'button' => [
                    'text' => 'Ver los videos',
                    'link' => route('voyager.videos.index'),
                    'class' => 'primary',
                ],
            'image' => '',
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', app(Video::class));
    }
}
