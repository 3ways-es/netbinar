<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Categoria extends Model
{

    public $additional_attributes = ['nombre_categoria_evento'];

    /**
     * Solo los videos publicados
     */
    public function videos() {
        if(substr((app('Illuminate\Routing\UrlGenerator')->full()),-7) == "preview")
            return $this->hasMany(Video::class);
        else
            return $this->hasMany(Video::class)->publicados();
    }

    /**
     * Tanto videos publicados como no
     */
    public function videosTodos() {
        return $this->hasMany(Video::class);
    }

    public function scopePublicados($query)
    {
        return $query->where('publicado', 1);
    }

    public function evento() {
        return $this->BelongsTo(Evento::class);
    }

    public function getNombreCategoriaEventoAttribute() {
        return "{$this->evento->titulo}: {$this->nombre}";
    }
}
