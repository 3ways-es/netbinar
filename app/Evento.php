<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Evento extends Model
{
    public function eventos_colaboradores() {
        return $this->hasMany(EventosColaboradore::class);
    }

    public function scopePublicados($query)
    {
        return $query->where('publicado', 1);
    }

    public function eventos_equipo() {
        return $this->hasMany(EventosEquipo::class);
    }
}
