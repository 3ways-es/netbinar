<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventosEquipo extends Model
{
    public $with = ['participante','evento'];

    public function participante() {
       return $this->belongsTo(Participante::class);
    }

    public function evento() {
        return $this->belongsTo(Evento::class);
     }
}
