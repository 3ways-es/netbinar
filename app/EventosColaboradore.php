<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class EventosColaboradore extends Model
{
    public function colaborador() {
        return $this->BelongsTo(Colaboradore::class);
    }
}
