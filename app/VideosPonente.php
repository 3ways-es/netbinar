<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Carlos Colmenarez
 * Modelo pivot "videos_ponentes"
 */
class VideosPonente extends Model
{
    public $with = ['participante'];
    
    public function participante() {
        return $this->belongsTo(Participante::class);
    }
}
