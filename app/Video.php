<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Voyager;

/**
 * Carlos Colmenarez
 * Modelo "videos"
 */

class Video extends Model
{
    protected $appends = ['month', 'day'];
    public $with = ['videos_coordinadores', 'videos_ponentes'];

    public function getMonthAttribute() {
        return \Carbon\Carbon::parse($this->fecha)->format('M');
    }

    public function getDayAttribute() {
        return \Carbon\Carbon::parse($this->fecha)->format('d');
    }
    public function videos_coordinadores() {
        return $this->hasMany(VideosCoordinadores::class);
    }
    public function videos_ponentes() {
        return $this->hasMany(VideosPonente::class);
    }
    public function categoria() {
        return $this->belongsTo(Categoria::class);
    }

    public function scopePublicados($query)
    {
        return $query->where('publicado', 1);
    }
}
