Para una instalación limpia ejecutar:

- "composer install".

- "npm install".

- Configurar archivo .env.

- Habilitar los seeders necesarios y ejecutar "php artisan migrate:fresh --seed".



Al hacer pull con los cambios realizar las siguientes acciones:

- Si hay cambios en "composer.json" ejecutar "composer install".

- Si hay cambios en las "migrations" ejecutar "php artisan migrate".

- Si hay cambios en los "seeders" comentar o descomentar los seeders que hagan faltan en "DatabaseSeeder.php" y ejecutar "php artisan db:seed".