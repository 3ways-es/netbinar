<?php

return [
    'message' => 'Este Sitio Web utiliza cookies de terceros para mejorar nuestros servicios analizando sus hábitos de navegación, lo que nos permite personalizar el contenido que ofrecemos y mostrarle publicidad relacionada con sus preferencias. Para aceptar su uso puede hacer click en el botón Aceptar. Nosotros usaremos su información obtenida a través de las cookies. Si quiere conocer mas detalle sobre como hacemos uso de las cookis consulte en el siguiente enlace.',
    'agree' => 'Aceptar Cookies',
];
