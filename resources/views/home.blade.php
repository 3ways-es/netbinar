{{-- CARLOS COLMENAREZ: VISTA HOME --}}
@extends('base')

@include('cookieConsent::index')
@section('header')
@include('menu')

    <div class="my-3 row logo pt-5">
        <div class="col-md-3 col-10 logo">
            <img class="img-respon" src="{{Voyager::image(setting('site.logo'))}}" alt="GETECCU" />
        </div>
    </div>
@endsection

@section('content')
<div id='app'>
    @foreach ($categorias as $categoria)
        @if ($categoria->mostrar_titulo == 1)
            @if($categoria->destacado == 1)

            @elseif($categoria->id != 3)
                <div id="highlights">
            @else
                <div id="takehome">
            @endif        
            <div class="container-fluid">
                <h3 class="titulo-categoria mt-5 titulo-categoria"><strong>{{strtoupper($categoria->nombre)}}</strong></h3>
            </div>
        @endif
        @if($categoria->videos->count() > 0)
            @if ($categoria->destacado == 1)
                    @include('destacado')
            @elseif($categoria->id != 3)
                <video-component categoria='{{$categoria}}'></video-component>            
            @else
                @include('take-home')
            @endif
        @else
        <div class="container-cabecera">
            <div class="cabecera row mx-0">
                <img src="img\banner_categorias_vacias.jpg">
            </div>
        </div>
        @endif
        @if ($categoria->mostrar_titulo == 1 && $categoria->destacado != 1)
            </div>
        @endif
    @endforeach
</div>
    {{-- @include('equipo') --}}
    <div id="historico">
        @include('historico')
    </div>
    @include('footer')
@endsection