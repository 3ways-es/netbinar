<div class="fondo-color-primario-opa10">
    <div class="container-fluid py-2">
        <div class="pb-5 mx-auto">
            <h3 class="titulo-categoria mt-5"><strong>HISTÓRICO</strong></h3>
            <div class="slider-historico row mx-0">
                @foreach ($eventos as $evento_h)
                    <a href="{{($evento_h->enlace) ? $evento_h->enlace : url()->current()."/".$evento_h->id}}" title="{{$evento_h->titulo}}"><img class="img-fluid" src="/storage/{{$evento_h->imagen}}" /></a>
                @endforeach
            </div>
        </div>
    </div>
</div>

<style>
    .slider-historico .slick-arrow:focus {
        outline: none;
    }

    .slider-historico .slick-arrow {
        position: absolute;
        height: 3em;
        width: 3em;
        top: 50%;
        transform: translateY(-50%);
        color: #fff;
        background-color: rgba({{colorHexToRGB(setting('site.color_primario'))}}, 0.9);
        border: none;
        z-index: 1;
    }

    .slider-historico .slick-next {
        right: 0;
    }

    .slider-historico .slick-prev {
        left: 0;
    }

    .slider-historico .slick-slide:not(.slick-active) {
        padding-right: 5px;
    }

    .slider-historico .slick-current {
        padding-right: 5px;
    }

    .slider-historico .slick-active:not(.slick-current) {
        padding-left: 5px;
    }

    @media (max-width: 550px) {
        .slider-historico .slick-next {
            right: 5px;
        }
    }
</style>