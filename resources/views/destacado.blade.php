

@foreach($categoria->videos as $video)
<div class="container-cabecera">
    <div class="cabecera row mx-0">
        <div class="overlay-compartir" id="overlayCompartir">
            
        </div>
        <div class="overlay-compartir-close" id="overlayCompartirCerrar">
            <button type="button" onclick="showOverlayCompartir('none')" class="btn text-white" aria-label="Close">
                <span aria-hidden="true"><i class="far fa-window-close fa-2x"></i></span>
            </button>
        </div>
        <div class="row overlay-compartir-botones" id="overlayCompartirBotones">
            <a class="text-white" href="http://www.facebook.com/sharer.php?u={{ Request::server ("SERVER_NAME") }}/videos/{{$video->id}}/&t=Mira%20&quot;{{$video->titulo}}&quot;%20en%20{{setting('site.title')}}">
                <button class="btn text-white" type="button">
                    <i class="fab fa-facebook-f"></i>
                </button>          
            </a>    
            <a class="text-white" href="https://twitter.com/intent/tweet?text=Mira+&quot;{{$video->titulo}}&quot;+en+{{setting('site.title')}}:&url=http://{{ Request::server ("SERVER_NAME") }}/videos/{{$video->id}}">
                <button class="btn text-white" type="button">
                    <i class="fab fa-twitter"></i>
                </button>
            </a>
            <a class="text-white" href="https://api.whatsapp.com/send?text=Mira%20&quot;{{$video->titulo}}&quot;%20en%20{{setting('site.title')}}:%20http://{{ Request::server ("SERVER_NAME") }}/videos/{{$video->id}}">
                <button class="btn text-white" type="button">
                    <i class="fab fa-whatsapp"></i>
                </button>
            </a>
        </div>
        <div class="col-md-9 col-12 p-0">
            @if($video->mostrar_imagen == 1)
                <div class="cabecera-video">
                    <img class="img-fluid" src="{{Voyager::image($video->imagen)}}">
                </div>
            @else
                <div class="embed-responsive embed-responsive-16by9 cabecera-video">
                    <iframe src="{{ $video->enlace }}"
                        class="iframe-video" frameborder="0" allow="autoplay; fullscreen" allowfullscreen>
                    </iframe>
                </div>
            @endif
        </div>
        <div class="overlay col-md-3 col-12">
            <div class="overlay-body px-1 pt-3">
                <h5 class="text-right">El Doctor <span class="text-secundario">Federico Pulido Ortega</span>, coordinador del proyecto, 
                    nos presenta <span class="text-primario">CROI Boston 2020</span> y el contenido que podemos seguir a través de <span class="text-primario">Congress TV</span>.</h5>
                <p class="text-right font-weight-light mt-3 mb-0 pb-2">
                    Disfruta de los contenidos<br>más relevantes del CROI 2020.
                </p>
                @if ($video->boton_compartir==1)
                    <div class="d-flex justify-content-end">
                        <button class="btn share-boton" onclick="showOverlayCompartir('block')">
                            <div class="d-flex row m-0 align-items-center">
                                <div class="col-2 pl-0"><i class="fas fa-share-alt fa-2x"></i></div>
                                <div class="col-8 pr-0">Compartir</div>
                            </div>
                        </button>
                    </div>
                @endif
            </div>
            <div class="overlay-footer">
                <div class="row mx-0">
                    <div class="col-6 text-left pl-2 pr-1">
                        <p class="mb-1 mt-1">UNA ACTIVIDAD DE:</p>
                        <img src="/img/gesidalogo.png" class="img-fluid">
                    </div>
                    <div class="col-6 text-right pl-1 pr-2">
                        <p class="mb-1 mt-1">CON LA COLABORACIÓN DE:</p>
                        @foreach ($evento->eventos_colaboradores as $colaboradores)
                            <img src="{{Voyager::image(App\Colaboradore::where('id',$colaboradores->colaboradore_id)->value('imagen'))}}" class="img-fluid">    
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
