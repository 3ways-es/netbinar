@extends('base')

@section('header')
    @include('menu')

    <div class="my-3 row logo pt-5">
        <div class="col-md-3 col-10 logo">
            <img class="img-respon" src="{{ Voyager::image(setting('site.logo')) }}" alt="{{ setting('site.name') }}" />
        </div>
    </div>
@endsection

@section('content')
    <div class="container mb-5">
        <h3 class="titulo-categoria my-5 titulo-categoria">
            <strong>POLÍTICA DE PRIVACIDAD</strong>
        </h3>

        <h4><strong>Responsable</strong></h4>
        <p><strong>1. Identidad</strong></p>
        <p>Empresa: GRUPO DE ESTUDIO DE LA SEIMC, S.L. &#8211; CIF: G85144442,, Dir. Postal: C/ Agustín de Betancourth, nº 13. – Ciudad: Madrid – CP: 28003, Teléfono: 91 556 80 25, E-Mail: <a href="mailto:secretaria-sg.org">secretaria-sg.org</a></p>
        <p>Datos de contacto del delegado en protección de datos:</p>
        <p>Herminia Esteban Martín.</p>

        <h4><strong>FINALIDADES</strong></h4>
        <p><strong>2. Descripción ampliada de la finalidad/es del tratamiento:</strong></p>
        <p>Trataremos sus datos para la Gestión administrativa, contable y fiscal.</p>

        <h4><strong>CONSERVACIÓN</strong></h4>
        <p><strong>3. Plazos o criterios de conservación de los datos:</strong></p>
        <p>Los datos personales proporcionados se conservarán mientras se mantenga la relación con la entidad y no se solicite su supresión por el interesado, serán conservados conforme a los plazos legales establecidos en materia fiscal y contable, tomando como referencia la última comunicación.</p>
        
        <h4><strong>DECISIONES</strong></h4>
        <p><strong>4. Decisiones automatizadas, perfiles y lógica aplicada:</strong></p>
        <p>La empresa NO tomará decisiones automatizadas, perfiles ó lógica aplicada a sus datos.</p>
        
        <h4><strong>LEGITIMACIÓN</strong></h4>
        <p><strong>5. Legitimación por ejecución de un contrato:</strong></p>
        <p>Debido a que el tratamiento es necesario para la ejecución de un contrato mercantil, en el que usted es parte , se hace constar que el tipo de contrato de que se trata, es el citado contrato mercantil o la relación precontractual.</p>
        <p>Como la comunicación de datos personales es un requisito legal o contractual y un requisito necesario para suscribir el citado contrato, se informa al interesado que está obligado a facilitar los datos personales, y así mismo que las consecuencias de no hacerlo pueden suponer la no prestación del servicio solicitado.</p>
        
        <h4><strong>DESTINATARIOS</strong></h4>
        <p><strong>6. Durante el periodo de duración del tratamiento, GRUPO DE ESTUDIO DE LA SEIMC, S.L. no realizará ninguna cesión, salvo obligación legal, ni tampoco transferencia alguna.</strong></p>
        
        <h4><strong>DERECHOS</strong></h4>
        <p><strong>7. El interesado puede ejercer los siguientes derechos:</strong></p>
        <ul>
            <li>Derecho a solicitar el acceso a sus datos personales.</li>
            <li>Derecho a solicitar su rectificación o supresión.</li>
            <li>Derecho a solicitar la limitación de su tratamiento.</li>
            <li>Derecho a oponerse al tratamiento.</li>
            <li>Derecho a la portabilidad de los datos.</li>
            <li>Derecho a retirar el consentimiento prestado.</li>
        </ul>
        <p>Cualquier persona tiene derecho a obtener confirmación sobre si en la Entidad estamos tratando datos personales que les conciernan, o no. Las personas interesadas tienen derecho a acceder a sus datos personales, así como a solicitar la rectificación de los datos inexactos o, en su caso, solicitar su supresión cuando, entre otros motivos, los datos ya no sean necesarios para los fines que fueron recogidos. En determinadas circunstancias, los interesados podrán solicitar la limitación del tratamiento de sus datos, en cuyo caso únicamente los conservaremos para el ejercicio o la defensa de reclamaciones. En determinadas circunstancias y por motivos relacionados con su situación particular, los interesados podrán oponerse al tratamiento de sus datos. En este caso, la entidad, dejará de tratar los datos, salvo por motivos legítimos imperiosos, o el ejercicio o la defensa de posibles reclamaciones. Si ha otorgado su consentimiento para alguna finalidad concreta, tiene derecho a retirar el consentimiento otorgado en cualquier momento, sin que ello afecte a la licitud del tratamiento basado en el consentimiento previo a su retirada. Para ello podrá emplear los formularios habilitados por la empresa, o bien dirigir un escrito a Empresa: GRUPO DE ESTUDIO DE LA SEIMC, S.L. &#8211; C/ Sánchez Díaz nº 15 Bajo, – CP: 28027 – Ciudad: Madrid, también puede enviar un email a: <a href="mailto:gesida@seimc.org">gesida@seimc.org</a></p>
        <p>En caso de que sienta vulnerados sus derechos en lo concerniente a la protección de sus datos personales, especialmente cuando no haya obtenido satisfacción en el ejercicio de sus derechos, puede presentar una reclamación ante la Autoridad de Control en materia de Protección de Datos competente a través de su sitio web: www.agpd.es.</p>
        <p>En cumplimiento de lo prevenido en el artículo 21 de la Ley 34/2002 de servicios de la sociedad de la información y comercio electrónico, si usted no desea recibir más información sobre nuestros servicios, puede darse de baja en la siguiente dirección de correo electrónico de la entidad, indicando en el asunto &#8220;No enviar correos&#8221;.</p>
        
        <p><strong>PROCEDENCIA DE LOS DATOS</strong></p>
        <p>Los datos personales que tratamos en GRUPO DE ESTUDIO DE LA SEIMC, S.L. proceden directamente de usted:</p>
        <p>Las categorías de datos que se tratan son:</p>
        <ul>
            <li>Datos de identificación.</li>
            <li>Códigos o claves de identificación.</li>
            <li>Direcciones postales o electrónicas.</li>
            <li>Información comercial.</li>
            <li>Datos económicos.</li>
            <li>No se tratan datos especialmente protegidos.</li>
        </ul>
        <p>No se tratan datos especialmente protegidos.</p>
    </div>

    @include('footer')
@endsection