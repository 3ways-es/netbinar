<style>
    .slick-track
    {
        margin-left: 0;
        margin-right: 0;
    }
</style>

<div class="container-fluid">
    <div id="highlights-web">
        <div class="row py-3 mx-0">

            @foreach($categoria->videos as $video)

            <div class="col-3">
                <div class="row px-1">
                    <div class="video-miniatura">
                    <img class="img-fluid mx-auto" src="{{Voyager::image($video->imagen)}}" alt="slide {{$loop->iteration}}" />
                        <div class="video-miniatura-overlay d-flex">
                            <div class="video-miniatura-play d-flex">
                                <button class="d-flex align-self-center video-miniatura-play-button mx-auto"><a
                                        href="video.html"><img src="{{Voyager::image(setting('site.play'))}}" class="img-fluid"/></a></button>
                            </div>
                            <div class="video-miniatura-share d-flex align-items-end justify-content-end">
                                <button class="btn d-flex video-miniatura-share-button mr-2 mb-1" type="button"
                                    data-toggle="collapse" data-target="#share{{$categoria->order}}{{$loop->iteration}}" aria-expanded="false"
                                    aria-controls="collapseExample"><i class="fas fa-share-alt"></i></button>
                            </div>
                            <div class="collapse video-share-button-4" id="share{{$categoria->order}}{{$loop->iteration}}">
                                <div class="row">
                                    <button class="btn text-white video-share-facebook" type="button"><i
                                            class="fab fa-facebook-f"></i></button>
                                </div>
                                <div class="row">
                                    <button class="btn text-white video-share-twitter" type="button"><i
                                            class="fab fa-twitter"></i></button>
                                </div>
                                <div class="row">
                                    <button class="btn text-white video-share-whatsapp" type="button"><i
                                            class="fab fa-whatsapp"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-none">
                        <div class="ponentes">
                            {{ \App\VideosPonente::where('video_id', $video->id)->get() }}
                        </div>
                    </div>
                </div>
                <div class="row pt-2">
                    <div class="col-3 px-1">
                        <div class="video-date">
                            <div class="video-month text-center px-0">
                            <span class="video-title">{{ strtoupper($video->Month) }}</span>
                            </div>
                            <div class="video-day text-center">
                                <span class="video-title">{{ $video->day }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-9 px-1">
                    <p class="video-title">{{ $video->titulo }}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <div class="cabecera row mx-0">
            <div class="col-md-9 p-0">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe src="https://player.vimeo.com/video/336812660?title=0&byline=0&portrait=0"
                        class="iframe-video" frameborder="0" allow="autoplay; fullscreen" allowfullscreen>
                    </iframe>
                </div>
            </div>
            <div class="video-overlay p-4 p-md-2 col-md-3">
                <div class="overlay-body">
                    <div class="row mt-2 mx-md-0 mb-md-0 p-y-md-1 p-x-md-3">
                        <div class="col-3 px-0">
                            <div class="video-date">
                                <div class="video-month text-center px-0">
                                    <span class="video-title">MAY</span>
                                </div>
                                <div class="video-day text-center">
                                    <span class="video-title">5</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-9 px-1">
                            <p class="video-title font-weight-bold text-white">Lorem ipsum, dolor sit amet
                                consectetur adipisicing elit.</p>
                        </div>
                    </div>
                    <div class="video-ponentes mx-0">
                        <div class="row mb-2 mx-3 mx-md-0 text-white d-flex align-items-center">
                            <div class="col-4 px-1">
                                <img class="video-overlay-img-blue" src="//via.placeholder.com/400x400?text=1"
                                    alt="slide 1" />
                            </div>
                            <div class="col-8 px-1 video-ponente-nombre">
                                <p class="mb-md-0"><span class="font-weight-bold">Lorem ipsum dolor sit amet
                                        consectetur,</span>
                                    adipisicing elit. Illo consectetur inventore</p>
                            </div>
                        </div>
                        <div class="row mb-2 mx-3 mx-md-0 text-white d-flex align-items-center">
                            <div class="col-4 px-1 d-flex align-items-center">
                                <img class="video-overlay-img-green" src="//via.placeholder.com/400x400?text=1"
                                    alt="slide 1" />
                            </div>
                            <div class="col-8 px-1 d-flex align-items-center video-ponente-nombre">
                                <p class="mb-md-0"><span class="font-weight-bold">Lorem ipsum dolor sit amet
                                        consectetur,</span>
                                    adipisicing elit. Illo consectetur inventore</p>
                            </div>
                        </div>
                        <div class="row mb-2 mx-3 mx-md-0 text-white d-flex align-items-center">
                            <div class="col-4 px-1 d-flex align-items-center">
                                <img class="video-overlay-img-green" src="//via.placeholder.com/400x400?text=1"
                                    alt="slide 1" />
                            </div>
                            <div class="col-8 px-1 d-flex align-items-center video-ponente-nombre">
                                <p class="mb-md-0"><span class="font-weight-bold">Lorem ipsum dolor sit amet
                                        consectetur,</span>
                                    adipisicing elit. Illo consectetur inventore</p>
                            </div>
                        </div>
                        <div class="row mb-2 mx-3 mx-md-0 text-white d-flex align-items-center">
                            <div class="col-4 px-1 d-flex align-items-center">
                                <img class="video-overlay-img-green" src="//via.placeholder.com/400x400?text=1"
                                    alt="slide 1" />
                            </div>
                            <div class="col-8 px-1 d-flex align-items-center video-ponente-nombre">
                                <p class="mb-md-0"><span class="font-weight-bold">Lorem ipsum dolor sit amet
                                        consectetur,</span>
                                    adipisicing elit. Illo consectetur inventore</p>
                            </div>
                        </div>
                        <div class="row mb-2 mx-3 mx-md-0 text-white d-flex align-items-center">
                            <div class="col-4 px-1 d-flex align-items-center">
                                <img class="video-overlay-img-green" src="//via.placeholder.com/400x400?text=1"
                                    alt="slide 1" />
                            </div>
                            <div class="col-8 px-1 d-flex align-items-center video-ponente-nombre">
                                <p class="mb-md-0"><span class="font-weight-bold">Lorem ipsum dolor sit amet
                                        consectetur,</span>
                                    adipisicing elit. Illo consectetur inventore</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="highlights-mobile">
        <div class="row py-3 mx-0 slider-mobile">
            @foreach($categoria->videos as $video)
            <div class="col">
                <div class="row px-1">
                    <div class="video-miniatura">
                        <img class="img-fluid mx-auto" src="{{Voyager::image($video->imagen)}}" alt="slide {{$loop->iteration}}"/>
                        <div class="video-miniatura-overlay d-flex">
                            <div class="video-miniatura-play d-flex">
                                <button class="d-flex align-self-center video-miniatura-play-button mx-auto">
                                    <a href="video.html"><img src="{{Voyager::image(setting('site.play'))}}" class="img-fluid" /></a>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-2">
                    <div class="col-3 px-1">
                        <div class="video-date">
                            <div class="video-month text-center px-0">
                                <span class="video-title">{{ strtoupper($video->Month) }}</span>
                            </div>
                            <div class="video-day text-center">
                                <span class="video-title">{{ $video->day }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-9 px-1">
                        <p class="video-title">{{ $video->titulo }}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>