@extends('base')

@section('style')
<style type="text/css">
    .bordered {
        border-bottom: 2px solid {{ setting('site.color_primario') }};
        padding-bottom: 20px
    }

    .text {
        z-index: 1;
        position: relative
    }

    .text h1 {
        font-weight: bold
    }

    .text p {
        font-size: 14pt
    }

    .text hr {
        width: 70%
    }

    #tool-icon {
        color: rgba(230, 229, 229, 0.6);
        position: absolute;
        right: 20px;
        bottom: 20px
    }
</style>
@endsection

@section('content')
<div class="container my-5">
    <div class="row m-0 bordered">
        <div class="col-sm-12 col-md-3 mt-5 mb-3">
            <img src="{{ Voyager::image(setting('site.logo')) }}" class="img-fluid" alt="{{ setting('site.name') }}">
        </div>
    </div>

    <div class="card my-5">
        <div class="card-body">
            <div class="text">
                <h1>Próximamente</h1>
                <br />
                <br />
                <br />
                <br />
                <br />
            </div>

            <i class="fas fa-10x fa-tools" id="tool-icon"></i>
        </div>
    </div>
</div>
@endsection