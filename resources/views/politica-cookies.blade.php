@extends('base')

@section('header')
    @include('menu')

    <div class="my-3 row logo pt-5">
        <div class="col-md-3 col-10 logo">
            <img class="img-respon" src="{{ Voyager::image(setting('site.logo')) }}" alt="{{ setting('site.name') }}" />
        </div>
    </div>
@endsection

@section('content')
    <div class="container mb-5">
        <h3 class="titulo-categoria my-5 titulo-categoria">
            <strong>POLÍTICA DE COOKIES</strong>
        </h3>

        <h3><strong>¡Bienvenido a GeSIDA!</strong></h3>
        <p>El objetivo de esta política es informar a los interesados acerca de las cookies que emplea esta página web de conformidad con lo establecido en la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico, y el Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016.</p>

        <h4><strong>1. Uso de cookies. ¿Qué son las cookies?</strong></h4>
        <p>Las cookies son ficheros que se descargan en su Ordenador, Smartphone o Tablet al acceder a determinadas páginas web. La utilización de cookies, ofrece numerosas ventajas en la prestación de servicios de la Sociedad de la Información, puesto que, entre otras: (a) facilita la navegación del usuario en el Sitio Web; (b) facilita al usuario el acceso a los diferentes servicios que ofrece el Sitio Web; (c) evita al usuario volver a configurar características generales predefinidas cada vez que accede al Sitio Web; (d) favorece la mejora del funcionamiento y de los servicios prestados a través del Sitio Web, tras el correspondiente análisis de la información obtenida a través de las cookies instaladas; (e) permiten a un Sitio Web, entre otras cosas, almacenar y recuperar información sobre los hábitos de navegación de un usuario o de su equipo y, dependiendo de la información que contengan y de la forma en que utilice su equipo, pueden utilizarse para reconocer al usuario.</p>
        <p>La mayoría de los navegadores aceptan como estándar a las cookies y, con independencia de las mismas, permiten o impiden en los ajustes de seguridad las cookies temporales o memorizadas.</p>
        <p>Tenga en cuenta que para poder utilizar y contar con una mejor experiencia de navegación,  es necesario que tenga habilitadas las cookies, especialmente aquellas de carácter técnico que resultan necesarias para que pueda identificarle como usuario registrado cada vez que acceda a la presente web.</p>
        <p>En caso de no querer recibir cookies, por favor, configure su navegador de internet, para que las borre del disco duro de su ordenador, las bloquee o le avise en su caso de instalación de las mismas.</p>

        <h4><strong>2. ¿Qué tipos de cookies utiliza esta página web?</strong></h4>
        <p><strong>Cookies utilizadas en este sitio web.</strong><br />
        Siguiendo las directrices de la Agencia Española de Protección de Datos, procedemos a detallar el uso de las cookies que esta página web emplea, con el fin de proporcionarle la máxima información posible.</p>
        <p><strong>Cookies Propias:</strong><br />
        Son aquellas que se envían al equipo terminal del usuario desde un equipo o dominio gestionado por el propio editor y desde el que se presta el servicio solicitado por el usuario.</p>
        <p>A continuación la lista de las que utilizamos:</p>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Tipo</th>
                        <th>Proveedor</th>
                        <th>Finalidad</th>
                        <th>Duración</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>laravel_session</td>
                        <td>Cookie de sesión</td>
                        <td>INTERACTIVE COMMUNICATION PROJECTS</td>
                        <td>Esta cookie es nativa de Laravel y permite a la web guardar datos serializados de estado. En esta web se usa para establecer sesiones de usuario pasando datos de estado a través de una cookie temporal también conocida como Cookie de sesión. </td>
                        <td>Hasta finalizar sesión</td>
                    </tr>
                    <tr>
                        <td>XSRF-TOKEN</td>
                        <td>Cookie de sesión</td>
                        <td>INTERACTIVE COMMUNICATION PROJECTS</td>
                        <td>Esta es una cookie encriptada que sirve para controlar que todos los envíos de formularios son realizados por el usuario actualmente en sesión, evitando ataques CSRF (Cross-Site Request Forgery).</td>
                        <td>Hasta finalizar sesión</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <p><strong>Cookies de Terceros:</strong><br />
        Son aquellas que se envían al equipo terminal del usuario desde un equipo o dominio que no es gestionado por el editor, sino por otra entidad que trata los datos obtenidos través de las cookies.</p>
        <p>A continuación la lista de las que utilizamos:</p>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Tipo</th>
                        <th>Proveedor</th>
                        <th>Finalidad</th>
                        <th>Duración</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>_ga</td>
                        <td>Cookies analíticas</td>
                        <td>Google</td>
                        <td>Se usa para distinguir a los usuarios.</td>
                        <td>2 años</td>
                    </tr>
                    <tr>
                        <td>_gid</td>
                        <td>Cookies analíticas</td>
                        <td>Google</td>
                        <td>Se usa para distinguir a los usuarios.</td>
                        <td>24 horas</td>
                    </tr>
                    <tr>
                        <td>_gat</td>
                        <td>Cookies analíticas</td>
                        <td>Google</td>
                        <td>Se usa para limitar el porcentaje de solicitudes. </td>
                        <td>1 minuto</td>
                    </tr>
                    <tr>
                        <td>_gat_gtag_UA_(property-id)</td>
                        <td>Cookies analíticas</td>
                        <td>Google</td>
                        <td>Se usa para limitar el porcentaje de solicitudes. </td>
                        <td>1 minuto</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <p>GRUPO DE ESTUDIO DE LA SEIMC no se hace responsable, en ningún caso ni del contenido ni de la veracidad de las políticas y/o condiciones de uso y privacidad de los terceros, incluidos, a través de los enlaces, en esta política.</p>

        <h4><strong>3. Consentimiento</strong></h4>
        <p>En relación con la utilización de cookies de este sitio web descritos en el apartado anterior, el usuario autoriza y consiente su uso de la siguiente forma: Cuando el usuario accede a nuestra página de la web, verá un aviso donde se indica que esta utiliza cookies, pudiendo el usuario aceptar el uso de estas pulsando el botón de aceptar y gestionar sus preferencias a través de la configuración del panel y/o de su navegador. Si el usuario no pulsa el botón aceptar, no configura su navegador ni el panel y sigue navegando por el sitio web de GRUPO DE ESTUDIO DE LA SEIMC utilizando sus servicios, el usuario acepta el uso que se hacen las cookies necesarias en esta página web.</p>

        <h4><strong>4. Cómo bloquear o eliminar las cookies instaladas</strong></h4>
        <p>Puede usted permitir, bloquear o eliminar las cookies instaladas en su equipo mediante la configuración de las opciones de su navegador. Puede encontrar información sobre cómo hacerlo, en relación con los navegadores más comunes en los links que se incluyen a continuación:</p>
        <ul class=" mt-5 mb-5 browser-icons d-flex justify-content-around">
            <li class="d-inline-block">
                <a href="https://support.microsoft.com/es-es/kb/278835" target="_blank"><br />
                    <img src="{{ asset('img/browser-icons/internet-explorer.svg') }}" width="50px" alt="Internet Explorer"><br />
                </a>
            </li>
            <li class="d-inline-block">
                <a href="http://support.google.com/chrome/bin/answer.py?hl=es&#038;answer=95647" target="_blank"><br />
                    <img src="{{ asset('img/browser-icons/chrome.svg') }}" width="50px" alt="Google Chrome"><br />
                </a>
            </li>
            <li class="d-inline-block">
                <a href="http://support.mozilla.org/es/kb/Borrar%20cookies" target="_blank"><br />
                    <img src="{{ asset('img/browser-icons/mozilla.svg') }}" width="50px" alt="Mozilla Firefox"><br />
                </a>
            </li>
            <li class="d-inline-block">
                <a href="http://support.apple.com/kb/ph5042" target="_blank"><br />
                    <img src="{{ asset('img/browser-icons/safari.svg') }}" width="50px" alt="Safari"><br />
                </a>
            </li>
        </ul>

        <h4><strong>5. Modificaciones</strong></h4>
        <p>La presente política de cookies puede verse modificada en función de las exigencias legales establecidas o con la finalidad de adaptar dicha política a las instrucciones dictadas por la Agencia Española de Protección de Datos.</p>
        <p>Por esta razón, aconsejamos a los usuarios que visiten periódicamente nuestra política de cookies.</p>
        <p>Cuando se produzcan cambios significativos en esta política, en la medida de nuestras posibilidades, comunicaremos a los usuarios estos cambios mediante un aviso en nuestra página web.</p>
        <p>Si tiene dudas acerca de esta política de cookies, puede contactar con GRUPO DE ESTUDIO DE LA SEIMC a través del siguiente correo electrónico: <a href="mailto:gesida@seimc.org">gesida@seimc.org</a></p>
    </div>

    @include('footer')
@endsection