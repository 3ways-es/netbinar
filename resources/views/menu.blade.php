<style>
    .navbar-custom {
        color: white;
    }

    .navbar {
        padding:0rem;
        margin-bottom:2rem;
    }

    .nav-transparent{
        background-color: transparent;
    }

    .back-button-nav{
        display:block;
        position: absolute;
        top:0;
        left: 0;
        background-color: transparent;
        border:none;
        color:white;
        margin-left: 10px;
        margin-top: 36px;
        font-size: 36px;
    }

    .back-button-nav i{
        text-shadow: 1px 1px 4px black;
    }

    .social-links{
        margin-left: auto;
    }

    @media (max-width: 767.98px){
        .social-links{
            margin-left: 0px;
        }
    }



</style>

<script src="https://kit.fontawesome.com/55959cb7f8.js" crossorigin="anonymous"></script>

    <nav class="navbar navbar-expand-md fixed-top navbar-custom py-0 fondo-color-primario">
        <a class="navbar-brand ml-2" href="{{setting('site.web_principal')}}">
            <i class="fas fa-home"></i> Volver a @php echo (null!==setting('site.nombre_web_principal')) ? setting('site.nombre_web_principal') : 'PRINCIPAL'; @endphp
        </a>
        <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="#navbar" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse flex-column" id="navbar">
            <ul class="navbar-nav w-100 px-3 fondo-color-primario">
                @if (Route::current()->getName() == "home")
                    <li class="container-fluid">   
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="navbar-brand " href="#highlights"><small>Highlights</small></a>
                            </li>
                            <li class="nav-item">
                                <a class="navbar-brand" href="#takehome"><small>Take-Home</small></a>
                            </li>
                            <li class="nav-item">
                                <a class="navbar-brand" href="#historico"><small>Histórico</small></a>
                            </li>
                        </ul>
                    <li>
                @endif
                <li class="nav-item social-links">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{setting('site.twitter')}}" target="blank"><i class="fab fa-twitter"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{setting('site.facebook')}}" target="blank"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{setting('site.linkedin')}}" target="blank"><i class="fab fa-linkedin-in"></i></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    
    <nav class="navbar navbar-expand-md fixed-top navbar-custom py-0">
        <ul class="navbar-nav w-100 nav-transparent px-3" id="back-nav">
            <li class="nav-item">
                <a class="navbar-brand" href="javascript:history.back()">
                    <div class="back-button-nav"><i class="fas fa-arrow-left"></i></div>
                </a>
            </li>
        </ul>
    </nav>
