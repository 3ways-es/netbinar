
@foreach ($categoria->videos as $video)
<div class="container-cabecera">    
    <div class="cabecera row mx-0">
        <div class="overlay-compartir" id="overlayCompartirTakeHome">
            
        </div>
        <div class="overlay-compartir-close" id="overlayCompartirTakeHomeCerrar">
            <button type="button" onclick="showOverlayTakeHome('none')" class="btn text-white" aria-label="Close">
                <span aria-hidden="true"><i class="far fa-window-close fa-2x"></i></span>
            </button>
        </div>
        <div class="row overlay-compartir-botones" id="overlayCompartirTakeHomeBotones">
            <a class="text-white" href="http://www.facebook.com/sharer.php?u={{ Request::server ("SERVER_NAME") }}/videos/{{$video->id}}/&t=Mira%20&quot;{{$video->titulo}}&quot;%20en%20{{setting('site.title')}}">
                <button class="btn text-white" type="button">
                    <i class="fab fa-facebook-f"></i>
                </button>          
            </a>    
            <a class="text-white" href="https://twitter.com/intent/tweet?text=Mira+&quot;{{$video->titulo}}&quot;+en+{{setting('site.title')}}:&url=http://{{ Request::server ("SERVER_NAME") }}/videos/{{$video->id}}">
                <button class="btn text-white" type="button">
                    <i class="fab fa-twitter"></i>
                </button>
            </a>
            <a class="text-white" href="https://api.whatsapp.com/send?text=Mira%20&quot;{{$video->titulo}}&quot;%20en%20{{setting('site.title')}}:%20http://{{ Request::server ("SERVER_NAME") }}/videos/{{$video->id}}">
                <button class="btn text-white" type="button">
                    <i class="fab fa-whatsapp"></i>
                </button>
            </a>
        </div>
        <div class="col-md-9 col-12 p-0">
            @if($video->mostrar_imagen == 1)
                <div class="cabecera-video">
                    <img class="img-fluid" src="{{Voyager::image($video->imagen)}}">
                </div>
            @else
                <div class="embed-responsive embed-responsive-16by9 cabecera-video">
                    <iframe id="take-home" src="{{ $video->enlace }}"
                        class="iframe-video" frameborder="0" allow="autoplay; fullscreen" allowfullscreen autoplay=1>
                    </iframe>
                </div>
            @endif
        </div>
        
        <div class="video-overlay p-4 p-md-2 col-md-3 col-12">
            <div class="overlay-body">   
                <div class="video-ponentes mx-0 mt-5">
                    <div>
                        @if(!empty($video->texto))
                            <div class="row mb-3 mx-3 mx-md-0 text-white d-flex align-items-center">
                                {!!$video->texto!!}
                            </div>
                        @endif
                        @foreach ($video->videos_coordinadores as $coordinador)    
                            <div class="row mb-3 mx-3 mx-md-0 text-white d-flex align-items-center">
                                <div class="col-4 px-1">
                                    <img class="video-overlay-img video-overlay-img-coordinador img-fluid rounded-circle" src="/storage/{{$coordinador->participante->imagen}}" alt="{{$coordinador->participante->nombre}}"/>
                                </div>  
                                <div class="col-8 px-1 video-ponente-nombre text-left">
                                    <p class="mb-0">
                                        <span class="font-weight-bold">
                                            {{ $coordinador->participante->nombre_completo }}
                                        </span>                                    
                                        <span>{!!$coordinador->participante->filiacion!!}</span>
                                    </p>
                                </div>                                
                            </div>
                        @endforeach
                    </div>
                    <div>
                        @foreach ($video->videos_ponentes as $ponente)    
                            <div class="row my-2 mx-3 mx-md-0 text-white d-flex align-items-center">
                                <div class="col-4 px-1">
                                    <img class="video-overlay-img video-overlay-img-ponente img-fluid rounded-circle" src="/storage/{{$ponente->participante->imagen}}" alt="{{$ponente->participante->nombre}}"/>
                                </div>
                                <div class="col-8 px-1 video-ponente-nombre text-left">
                                    <p class="mb-0">
                                        <span class="font-weight-bold">
                                            {{ $ponente->participante->nombre_completo }}
                                        </span>                                        
                                        <span>{!!$ponente->participante->filiacion!!}</span>
                                    </p>
                                </div>
                            </div>                        
                        @endforeach
                    </div>
                </div>
                @if ($video->boton_compartir==1)
                    <div class="boton-take-home">
                        <button class="btn share-boton" onclick="showOverlayTakeHome('block')">
                            <div class="d-flex row m-0 align-items-center">
                                <div class="col-2 pl-0"><i class="fas fa-share-alt fa-2x"></i></div>
                                <div class="col-8 pr-0">Compartir</div>
                            </div>
                        </button>
                    </div>
                @endif
            </div>
        </div>    
    </div>
</div>
@endforeach