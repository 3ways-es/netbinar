@extends('base')

@section('header')
    @include('menu')
@endsection

@section('style')
<style type="text/css">

    .back-button{
        display: none;
    }
    @media (max-width: 767.98px) {
        header{
            display: none;
        }

        .back-button{
            display:block;
            position: absolute;
            top:0;
            left: 0;
            background-color: transparent;
            border:none;
            color:white;
            margin-left: 2.5vw;
            margin-top: 2vw;
            font-size: 7vw;
            padding: 1vw;
        }

        #back-nav{
            display: none;
        }

        .back-button i{
            text-shadow: 1px 1px 4px black;
        }


        .video-miniatura-share-button {
            font-size: 5vw;
            padding: 2.5vw;
        }

        .video-overlay{
            position:relative;
        }
        .video-miniatura-play-button {
            max-width: 25vw;
        }

        .video-ponente-nombre{
            font-size: 3.5vw;
        }

        /* Scrollbar styles */
        ::-webkit-scrollbar {
        width: 0px;
        height: 0px;
        }

    }
    .video-share-button-4 button {
        font-size: 18px;
        padding: 7.2px 10.8px;
    }

</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="cabecera mt-0 mt-md-5 row mx-0">
        <div class="overlay-compartir" id="overlayCompartir">
            
        </div>
        <div class="overlay-compartir-close" id="overlayCompartirCerrar">
            <button type="button" onclick="showOverlayCompartir('none')" class="btn text-white" aria-label="Close">
                <span aria-hidden="true"><i class="far fa-window-close fa-2x"></i></span>
            </button>
        </div>
        <div class="row overlay-compartir-botones" id="overlayCompartirBotones">
            <a class="text-white" href="http://www.facebook.com/sharer.php?u={{ Request::server ("SERVER_NAME") }}/videos/{{$video->id}}/&t=Mira%20&quot;{{$video->titulo}}&quot;%20en%20{{setting('site.title')}}">
                <button class="btn text-white" type="button">
                    <i class="fab fa-facebook-f"></i>
                </button>          
            </a>    
            <a class="text-white" href="https://twitter.com/intent/tweet?text=Mira+&quot;{{$video->titulo}}&quot;+en+{{setting('site.title')}}:&url=http://{{ Request::server ("SERVER_NAME") }}/videos/{{$video->id}}">
                <button class="btn text-white" type="button">
                    <i class="fab fa-twitter"></i>
                </button>
            </a>
            <a class="text-white" href="https://api.whatsapp.com/send?text=Mira%20&quot;{{$video->titulo}}&quot;%20en%20{{setting('site.title')}}:%20http://{{ Request::server ("SERVER_NAME") }}/videos/{{$video->id}}">
                <button class="btn text-white" type="button">
                    <i class="fab fa-whatsapp"></i>
                </button>
            </a>
        </div>
        <div class="col-md-9 col-12 p-0">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe src="{{$video->enlace}}?autoplay=1&loop=0&autopause=0" class="iframe-video"
                    frameborder="0" allow="autoplay; fullscreen" allowfullscreen>
                </iframe>
                <a class="text-white" href="javascript:history.back()"><button class="back-button"><i class="fas fa-arrow-left"></i></button></a>
            </div>
        </div>
        <div class="video-overlay p-4 p-md-2 col-md-3 col-12">
            <div class="overlay-body">
                <div class="row mt-1 mt-md-2 mb-4 mb-md-2 mx-3 mx-md-1 d-flex align-items-center">
                    <div class="col-2 px-0">
                        <div class="video-date">
                            <div class="video-month text-center px-0">
                                <span class="video-month">{{ strtoupper($video->month) }}</span>
                            </div>
                            <div class="video-day text-center">
                                <span class="video-day"><strong>{{ $video->day }}</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-10 px-1">
                        <p class="video-title mb-md-0 font-weight-bold text-white">{{ $video->titulo }}</p>
                    </div>
                </div>
                <div class="video-ponentes">
                    @foreach ($video->videos_coordinadores as $vcoordinador)
                        <div class="row my-2 mx-3 mx-md-0 text-white d-flex align-items-center">
                            <div class="col-4 px-1">
                                <img class="video-overlay-img video-overlay-img-coordinador img-fluid rounded-circle" src="/storage/{{$vcoordinador->participante->imagen}}" alt="Ponente 1"/>
                            </div>
                            <div class="col-8 px-1 video-ponente-nombre text-left">
                                <p class="mb-0"><span class="font-weight-bold">{{$vcoordinador->participante->nombre_completo}}</span>{!!$vcoordinador->participante->filiacion!!}</p>
                            </div>
                        </div>
                    @endforeach
                    @foreach ($video->videos_ponentes as $vponente)
                        <div class="row my-2 mx-3 mx-md-0 text-white d-flex align-items-center">
                            <div class="col-4 px-1">
                                <img class="video-overlay-img video-overlay-img-ponente img-fluid rounded-circle" src="/storage/{{$vponente->participante->imagen}}" alt="Ponente 1"/>
                            </div>
                            <div class="col-8 px-1 video-ponente-nombre text-left">
                                <p class="mb-0"><span class="font-weight-bold">{{$vponente->participante->nombre_completo}}</span>{!!$vponente->participante->filiacion!!}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row d-flex align-items-center justify-content-center my-4 my-md-3">
                    <div class="col">
                        <button class="btn share-boton" onclick="showOverlayCompartir('block')"><i class="fas fa-share-alt"></i>&nbsp;&nbsp;&nbsp;Compartir</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">

{{-- CARLOS COLMENAREZ: VIDEOS RELACIONADOS --}}
@if($videos->count()>0)

    <div id="highlights-web">
        <h3 class="titulo-categoria mt-5">HIGHLIGHTS</h3>
        <div class="row py-3 mx-0 mb-3">
            
            @foreach ($videos as $v)
            <div class="col-3">
                <div class="row px-1">
                    <div class="video-miniatura">
                        <img class="img-fluid mx-auto" src="/storage/{{$v->imagen}}" alt="slide 4" />
                        <div class="video-miniatura-overlay d-flex">
                            <div class="video-miniatura-play d-flex">
                                <a href="/videos/{{$v->id}}" class="d-flex">
                                    <button class="d-flex align-self-center video-miniatura-play-button mx-auto">
                                        <img src="/img/play_button.png" class="img-fluid"/>
                                    </button>
                                </a>
                            </div>
                            <div class="video-miniatura-share d-flex align-items-end justify-content-end">
                                <button class="btn d-flex video-miniatura-share-button mr-2 mb-1" onclick="showButtons('share{{$v->id}}')" type="button">
                                    <i class="fas fa-share-alt"></i>
                                </button>
                            </div>
                            <div class="video-share-button-4" id="share{{$v->id}}">
                                <div class="row">
                                    <a class="text-white" href="http://www.facebook.com/sharer.php?u={{ Request::server ("SERVER_NAME") }}/videos/{{$v->id}}/&t=Mira%20&quot;{{$v->titulo}}&quot;%20en%20{{setting('site.title')}}">
                                        <button class="btn text-white" type="button">
                                            <i class="fab fa-facebook-f"></i>
                                        </button>
                                    </a>    
                                </div>
                                <div class="row">          
                                    <a class="text-white" href="https://twitter.com/intent/tweet?text=Mira+&quot;{{$v->titulo}}&quot;+en+{{setting('site.title')}}:&url=http://{{ Request::server ("SERVER_NAME") }}/videos/{{$v->id}}">
                                        <button class="btn text-white" type="button">
                                            <i class="fab fa-twitter"></i>
                                        </button>
                                    </a>
                                </div>
                                <div class="row">
                                    <a class="text-white" href="https://api.whatsapp.com/send?text=Mira%20&quot;{{$v->titulo}}&quot;%20en%20{{setting('site.title')}}:%20http://{{ Request::server ("SERVER_NAME") }}/videos/{{$v->id}}">
                                        <button class="btn text-white" type="button">
                                            <i class="fab fa-whatsapp"></i>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pt-2">
                    <div class="col-3 px-1">
                        <div class="video-date">
                            <div class="video-month text-center px-0">
                                <span class="video-month"> {{strtoupper($v->month)}}</span>
                            </div>
                            <div class="video-day text-center">
                                <span class="video-day"><strong>{{$v->day}}</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-9 px-1">
                        <p class="video-title">{{$v->titulo}}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <div id="highlights-mobile">
        <h3 class="text-blue mt-5 mx-3">HIGHLIGHTS</h3>
        
        @foreach ($videos as $v)
        <div class="d-flex justify-content-center row mx-0">
            <div class="col-12 px-3 mb-3">
                <div class="row mx-0 d-flex justify-content-center">
                    <div class="video-miniatura">
                    <img class="img-fluid mx-auto" src="/storage/{{ $v->imagen }}" alt="{{$v->titulo}}" />
                        <div class="video-miniatura-overlay d-flex">
                            <div class="video-miniatura-play d-flex">
                                <a href="/videos/{{$v->id}}">
                                    <button class="d-flex align-self-center video-miniatura-play-button mx-auto">
                                        <img src="/img/play_button.png" class="img-fluid" />
                                    </button>
                                </a>
                            </div>
                            <div class="video-miniatura-share d-flex align-items-end justify-content-end">
                                <button class="btn d-flex video-miniatura-share-button mr-2 mb-1" onclick="showButtons('shareMobile{{$v->id}}')" type="button">
                                    <i class="fas fa-share-alt"></i>
                                </button>
                            </div>
                            <div class="video-share-button-4" id="shareMobile{{$v->id}}">
                                <div class="row">
                                    <a class="text-white" href="http://www.facebook.com/sharer.php?u={{ Request::server ("SERVER_NAME") }}/videos/{{$v->id}}/&t=Mira%20&quot;{{$v->titulo}}&quot;%20en%20{{setting('site.title')}}">
                                        <button class="btn text-white" type="button">
                                            <i class="fab fa-facebook-f"></i>
                                        </button>
                                    </a>    
                                </div>
                                <div class="row">          
                                    <a class="text-white" href="https://twitter.com/intent/tweet?text=Mira+&quot;{{$v->titulo}}&quot;+en+{{setting('site.title')}}:&url=http://{{ Request::server ("SERVER_NAME") }}/videos/{{$v->id}}">
                                        <button class="btn text-white" type="button">
                                            <i class="fab fa-twitter"></i>
                                        </button>
                                    </a>
                                </div>
                                <div class="row">
                                    <a class="text-white" href="https://api.whatsapp.com/send?text=Mira%20&quot;{{$v->titulo}}&quot;%20en%20{{setting('site.title')}}:%20http://{{ Request::server ("SERVER_NAME") }}/videos/{{$v->id}}">
                                        <button class="btn text-white" type="button">
                                            <i class="fab fa-whatsapp"></i>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mx-0 pt-2">
                    <div class="col-2 px-1">
                        <div class="video-date">
                            <div class="video-month text-center px-0">
                                <span class="video-title">{{strtoupper($v->month)}}</span>
                            </div>
                            <div class="video-day text-center">
                                <span class="video-title"><strong>{{$v->day}}</strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-10 px-1">
                        <p class="video-title">{{$v->titulo}}</p>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    
@endif
{{-- CARLOS COLMENAREZ: FIN VIDEOS RELACIONADOS --}}
</div>
@include('footer')
@endsection