<footer id="footer-web" class="fondo-color-secundario">
    <div class="container-fluid">
    <div class="footer py-3 row m-0 d-flex align-items-center">
        <div class="col-md-3 col-6">
            <img src="{{ asset('img/logo-gesida.png') }}" class="img-fluid">
        </div>
        <div class="col-md-3 col-12 d-flex justify-content-center">
            <!-- Space -->
        </div>
        <ul class="col-md-6 col-6 p-0 d-flex list-unstyled">
            <li class="px-2 mx-2">
                <a href="{{ route('aviso-legal') }}" class="social-link" title="Aviso legal">Aviso legal</a>
            </li>
            <li class="px-2 mx-2">
                <a href="{{ route('politica-cookies') }}" class="social-link" title="Política de cookies">Política de cookies</a>
            </li>
            <li class="px-2 mx-2">
                <a href="{{ route('politica-privacidad') }}" class="social-link" title="Política de privacidad">Política de privacidad</a>
            </li>
        </ul>
    </div>    
    </div>
</footer>

<footer id="footer-mobile">
    <div class="footer fondo-color-secundario">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="col-6 my-4">
                <img src="{{ asset('img/logo-gesida.png') }}" class="img-fluid">
            </div>

            <ul class="col-12 p-0 d-flex align-items-center justify-content-center list-unstyled">
                <li class="px-2">
                    <a href="{{ route('aviso-legal') }}" class="social-link" title="Aviso legal">Aviso legal</a>
                </li>
                <li class="px-2">
                    <a href="{{ route('politica-cookies') }}" class="social-link" title="Política de cookies">Política de cookies</a>
                </li>
                <li class="px-2">
                    <a href="{{ route('politica-privacidad') }}" class="social-link" title="Política de privacidad">Política de privacidad</a>
                </li>
            </ul>
        </div>
    </div>
</footer>