<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">

    @if(setting('site.google_analytics_tracking_id') !== null)
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{setting('site.google_analytics_tracking_id')}}"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '{{setting('site.google_analytics_tracking_id')}}');
        </script>
    @endif

<title>{{setting('site.title')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato"/>

    @if (setting('site.font_links') !== null)
        {!!setting('site.font_links')!!}    
    @endif

    <style type="text/css">
        .fondo-color-primario {
            background-color: {{setting('site.color_primario')}};
        }
        .fondo-color-primario-opa10 {
            background-color: rgb({{colorHexToRGB(setting('site.color_primario'))}}, 0.1);
        }
        .fondo-color-primario-opa70 {
            background-color: rgb({{colorHexToRGB(setting('site.color_primario'))}}, 0.7);
        }
        .fondo-color-secundario {
            background-color: {{setting('site.color_secundario')}};
        }

        body {
            background-image: url('{{Voyager::Image(setting('site.imagen_fondo'))}}');
            background-repeat: no-repeat;
            background-size: cover;
            font-family: Lato, sans-serif;
            min-width: 320px;
            margin: auto;
        }

        /* Cabecera needed to position the button. Adjust the width as needed */
        .cabecera {
            position: relative;
            width: 100%;
            margin-top: 10px;
            margin-bottom: 25px;
        }

        .container-fluid {
            width: 85%;
            max-width: 1224px;
        }

        .navbar-custom a {
            color: rgba(255, 255, 255, 1);
        }

        /* Make the image responsive */
        .img-respon {
            width: 100%;
            height: auto;
        }

        .img-fluid {
            max-width: 100%;
            height: auto;
            width: 100%;
        }

        .overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            /* White see-through */
            background: rgba(245, 245, 245, 0.9);
            text-align: center;
        }

        .overlay h5 {
            font-size: 1.25vw;
        }

        .overlay p {
            font-size: .9vw;
        }

        .overlay-footer {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            height: 25%;
        }

        .overlay-footer p {
            font-size: .55vw;
        }

        .highlights-mobile,
        #highlights-mobile,
        /* #conocenos-mobile, */
        #historico-mobile,
        #footer-mobile {
            display: none;
        }

        .video-container {
            position: relative;
            width: 100%;
            margin-top: 10px;
            margin-bottom: 25px;
        }

        .video-overlay {
            position: relative;
            top: 0;
            bottom: 0;
            right: 0;
            /* Black see-through */
            background: rgba(0, 0, 0, 0.7);
            text-align: center;
        }

        .video-ponentes {
            overflow-y: auto;
        }

        .video-ponente-nombre {
            font-size: .8vw;
        }

        #conocenos-web{
        /* #conocenos-mobile { */
            color: white;
        }

        .equipo-overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            width: 100%;
            height: 75%;
            /* Blue see-through */
            background: rgba(0, 104, 196, 0.5);
            text-align: center;
        }

        .equipo-overlay-footer {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            width: 100%;
            height: 25%;
            /* Green see-through */
            background: rgba(36, 109, 2, 0.65);
            font-size: 1vw;
        }

        .equipo-tipo-trabajador {
            position: absolute;
            bottom: 5px;
            left: 1vw;
            font-size: 1.2vw;
        }

        body {
            color: #2a2a2a;
        }

        .titulo-categoria {
            color: {{setting('site.color_primario')}};
        }

        .text-primario {
            color: {{setting('site.color_primario')}};
        }

        .text-secundario {
            color: {{setting('site.color_secundario')}};
        }

        .container-cabecera {
            width: 85%;
            max-width: 1224px;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        /* Style the button and place it in the middle of the container/image */
        .share-boton {
            background-color: {{setting('site.color_primario')}};
            color: white;
            cursor: pointer;
            border-radius: 3px;
            font-size: 1.2vw;
            padding: .5vw 1.5vw;
            min-width: 120px;
        }

        .share-boton:hover {
            background-color: rgb({{colorHexToRGB(setting('site.color_primario'))}}, 0.9);
        }

        .overlay-compartir {
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            background: {{setting('site.color_primario')}};
            box-shadow: inset 0 0 100vw 100vh rgba(0, 0, 0, 0.45);
            /* filter: brightness(55%); */
            opacity: .9;
            text-align: center;
            z-index: 1;
            display: none;
            animation-name: fadeInOpacity;
            animation-iteration-count: 1;
            animation-timing-function: ease-in;
            animation-duration: 0.3s;
        }

        @keyframes fadeInOpacity {
            0% {
                opacity: 0;
            }
            100% {
                opacity: .9;
            }
        }

        .overlay-compartir-close{
            position: absolute;
            top: 5px;
            right: 5px;
            z-index: 2;
            display: none;
            animation-name: fadeInOpacity;
            animation-iteration-count: 1;
            animation-timing-function: ease-in;
            animation-duration: 1s;
        }

        .overlay-compartir-close button{
            font-size: 2vw;
        }

        .overlay-compartir-botones {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-45%, -50%);
            z-index: 2;
            display: none;
            animation-name: fadeInOpacity;
            animation-iteration-count: 1;
            animation-timing-function: ease-in;
            animation-duration: 1s;
        }

        .overlay-compartir-botones button {
            background-color: transparent;
            margin-left: 1vw;
            margin-right: 1vw;
            font-size: 5vw; 
        }

        .video-miniatura {
            position: relative;
        }

        .video-miniatura-overlay {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            /* Black see-through */
            background: rgba(0, 0, 0, 0.4);
            text-align: center;
            width: 100%;
        }

        .video-miniatura-play {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            height: 100%;
        }

        .video-miniatura-share {
            position: absolute;
            right: 0;
            bottom: 0;
        }

        .video-miniatura-play-button {
            color: white; 
            border: none;
            background-color: transparent;
            cursor: pointer;
            max-width: 8vw;
            transform: translate(-50%, -50%);
            position: absolute;
            top: 50%;
            left: 50%;
        }

        .video-miniatura-share-button {
            color: white;
            border: 1px solid white;
            border-radius: 50%;
            padding: .6vw;
            background-color: transparent;
            font-size: 1vw;
            cursor: pointer;
            text-align: center;
        }

        .video-miniatura-share-button i{
            display: inline-block;
            top: 50%;
            left: 50%;
            position: relative;
            transform: translate(-50%, 0%);
        }

        .video-miniatura-share-button:hover {
            color: {{setting('site.color_secundario')}};
            border: 1px solid {{setting('site.color_secundario')}};
        }

        .video-share-button-5 {
            position: absolute;
            right: 0;
            margin-top: 15%;
            margin-right: 10%;
        }

        .video-share-button-5 button {
            background-color: transparent;
            border: none;
            font-size: 1vw;
            padding: .05vw 1vw;
        }

        .video-share-button-4 {
            position: absolute;
            right: 0;
            margin-top: 8%;
            margin-right: 6%;
            display:none;
        }

        .video-share-button-4 button {
            background-color: transparent;
            border: none;
            font-size: 1.5vw;
            padding: 0vw .5vw;
        }

        .video-share-button-4 a button {
            background-color: transparent;
            border: none;
            font-size: 1.5vw;
            padding: 0vw .5vw;
        }

        .video-share-button-4 a {
            display: block;
            height: 2vw;
        }

        .video-date {
            border: 1px solid #2a2a2a;
            border-radius: 5px;
        }

        .video-month {
            background-color: {{setting('site.color_primario')}};
            border-radius: 5px 5px 0 0;
            color: white;
            font-size: 1.1vw;
        }

        .video-title {
            font-size: 1.2vw;
        }

        .video-day {
            background-color: white;
            border-radius: 0 0 5px 5px;
            font-size: 1.1vw;
        }

        .video-overlay-img {
            object-fit: cover;
            object-position: 0% 0%;
            width: 5vw;
            height: 5vw;
        }

        .video-overlay-img-coordinador {
            border: .3vw solid {{setting('site.color_primario')}};
        }

        .video-overlay-img-ponente {
            border: .3vw solid {{setting('site.color_secundario')}};
        }

        .text-white {
            color: white;
        }

        #historico-web .container-fluid,
        #historico-mobile .container-fluid {
            width: 100%;
            background-color: #eef4fa;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .container-historico {
            width: 75%;
        }

        .footer {
            width: 100%;
            min-height: 180px;
            background-repeat: no-repeat;
            background-size: cover;
            color: white;
        }

        .footer p {
            font-size: 1.5vw;
        }

        .social-link {
            color: white;
            opacity: 1;
            font-size: 1vw;
        }

        /* Scrollbar styles */
        ::-webkit-scrollbar {
            width: 3px;
            height: 12px;
        }

        ::-webkit-scrollbar-track {
            background: #f5f5f5;
            border-radius: 10px;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 10px;
            background: #ccc;
        }

        ::-webkit-scrollbar-thumb:hover {
            background: #999;
        }

        .buttonClose{
            border:none;
            background-color: transparent;
            color:white;
            font-size: 1.2vw;
        }

        .buttonClose:hover{
            color: rgba(245, 245, 245, 0.6)
        }

        .flecha-abajo::before {
            content: "";
            position: absolute;
            width: 0;
            height: 0;
            left: 50%;
            bottom: 0;
            transform: translate(-50%, 100%);
            border-width: 2vw 3vw 0;
            border-style: solid;
            border-color: {{setting('site.color_primario')}} transparent;
        }

        .marco {
            position: relative;
            border: 0.28vw solid {{setting('site.color_primario')}};
        }

        /* Enter and leave animations can use different */
        /* durations and timing functions.              */
        .slide-fade-enter-active {
            transition: all .5s ease;
        }

        .slide-fade-leave-active {
            transition: all .5s cubic-bezier(1.0, 0.5, 0.8, 1.0);
        }

        .slide-fade-enter, .slide-fade-leave-to
        /* .slide-fade-leave-active below version 2.1.8 */ {
            transform: translateY(-100px);
            opacity: 0;
        }

        .boton-take-home{
            position: relative;
            top: 50%;
            left: 50%;
            transform: translate(-50%, 0%);
            padding-top: 30px;
            padding-bottom: 30px;
        }
        
        @media (max-width: 767.98px) {

            .container-cabecera {
                width: 100%;
                padding-right: 0px;
                padding-left: 0px;
            }

            .container-fluid {
                width: 100%;
                padding-right: 0px;
                padding-left: 0px;
            }

            .logo {
                margin: auto;
            }

            .cabecera-video {
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                bottom: 0;
                right: 0;
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
            }

            iframe {
                width: 100%;
                height: 100%;
            }

            .overlay {
                position: relative;
            }

            .overlay h5 {
                font-size: 5vw;
            }

            .overlay p {
                font-size: 4vw;
            }

            .overlay-footer {
                position: relative;
                margin-top: 15px;
                margin-bottom: 15px;
            }

            .share-boton {
                font-size: 4vw;
                padding: 1.5vw 4.5vw;                
                min-width: 200px;
            }

            .overlay-footer p {
                font-size: 3vw;
            }

            #highlights-web,
            .highlights-web,
            /* #conocenos-web, */
            #historico-web,
            #footer-web {
                display: none;
            }

            #highlights-mobile,
            .highlights-mobile,
            /* #conocenos-mobile, */
            #historico-mobile,
            #footer-mobile {
                display: block;
            }

            .highlights-mobile h3 {
                margin-left: 5vw;
                font-size: 8vw;
            }

            /* #conocenos-mobile h3 {
                margin-left: 5vw;
                font-size: 6vw;
            } */

            #historico-mobile h3 {
                margin-left: 5vw;
                font-size: 6vw;
            }

            .video-miniatura-play-button {
                max-width: 15vw;
            }

            .video-date {
                height: 42px;
            }

            .video-month {
                font-size: 2vw;
                padding: 0;
                height: 20px;
                display: flex;
                align-items: center;
                width: 100%;
                justify-content: center;
            }

            .video-title {
                font-size: 4vw;
            }

            .video-day {
                font-size: 2.5vw;
                height: 20px;
                display: flex;
                align-items: center;
                width: 100%;
                justify-content: center;
            }

            .video-ponente-nombre {
                font-size: 3.5vw;
            }

            .equipo-overlay-footer {
                font-size: 2.5vw;
            }

            .equipo-tipo-trabajador {
                font-size: 3vw;
            }

            .container-historico {
                width: 100%;
            }

            .footer p {
                font-size: 2.5vw;
            }

            .social-link {
                font-size: 3vw;
            }

            .overlay-compartir-close button{
                font-size: 3.5vw;
            }

            .titulo-categoria {
                margin-left: 10px;
            }

            #conocenos-web .slick-slide {
                min-height: 50vw;
            }

            .overlay-compartir-botones{
                justify-content: center;
            }

            .overlay-compartir-botones button {
                font-size: 20vw;
                padding-left: 10vw;
                padding-right: 10vw;             
            }

            .video-overlay-img{
                width: 15vw;
                height: 15vw;
            }

            .video-overlay-img-coordinador {
                border: .75vw solid {{setting('site.color_primario')}};
            }

            .video-overlay-img-ponente {
                border: .75vw solid {{setting('site.color_secundario')}};
            }

            .video-share-button-4 a {
                height: 8vw;
            }

            .video-share-button-4 a button{
                font-size: 6vw;
            }
        }

        @media (min-width: 768px) and (max-width: 1023.98px) {
            .video-ponentes {
                max-height: 17vw;
            }

            .video-share-button-4 {
                margin-top: 7%;
                margin-right: 9%;
            }

            .equipo-tipo-trabajador{
                font-size: 2vw;
            }

            .equipo-overlay-footer{
                font-size: 1.6vw;
            }
        }

        @media (min-width: 1024px) and (max-width: 1439.99px) {
            .video-ponentes {
                max-height: 19vw;
            }

            .video-share-button-4 {
                margin-top: 8%;
                margin-right: 7%;
            }
        }

        .col-3-lg {
            flex: 0 0 26.5%;
            max-width: 26.5%;
            position: relative;
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            transition: all .5s ease;
        }

        .col-3-sm {
            transition: all .5s ease;
            flex: 0 0 24.5%;
            max-width: 24.5%;
            position: relative;
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
        }

        @media (min-width: 1440px) {
            .overlay h5 {
                font-size: 18px;
            }

            .overlay p {
                font-size: 13px;
            }

            .overlay-footer p {
                font-size: 8px;
            }

            .video-ponente-nombre {
                font-size: 11.5px;
            }

            .equipo-overlay-footer {
                font-size: 14.4px;
            }

            .equipo-tipo-trabajador {
                left: 14.4px;
                font-size: 17.3px;
            }

            .share-boton {
                font-size: 14.4px;
                padding: 7.2px 21.6px;
            }

            .video-month {
                font-size: 15.85px;
            }

            .video-title {
                font-size: 17.3px;
            }

            .video-day {
                font-size: 15.85px;
            }

            .footer p {
                font-size: 21.6px;
            }

            .social-link {
                font-size: 14.4px;
            }

            .video-miniatura-play-button {
                max-width: 115.2px;
            }

            .video-miniatura-share-button {
                font-size: 14.4px;
                padding: 8.64px;
            }

            .video-ponentes {
                max-height: 288px;
            }

            .video-share-button-4 button {
                font-size: 18px;
                padding: .72px 10.8px;
            }

            .video-share-button-4 a button {
                font-size: 18px;
                padding: .72px 10.8px;
            }

            .flecha-abajo::before {
                border-width: 28.8px 43.2px 0;
            }

            .marco {
                transition: all .5s ease;
                border: 4px solid {{setting('site.color_primario')}};
            }

            .overlay-compartir-botones button {
                margin-left: 14.4px;
                margin-right: 14.4px;
                font-size: 72px; 
            }

            .video-overlay-img{
                width: 72px;
                height: 72px;
            }

            .video-overlay-img-coordinador {
                border: 4.32px solid {{setting('site.color_primario')}};
            }

            .video-overlay-img-ponente {
                border: 4.32px solid {{setting('site.color_secundario')}};
            }

            .video-share-button-4 a {
                display: block;
                height: 32px;
            }
        }
        .footer .row {
            margin-right: auto;
            margin-left: auto;
        }

        .slick-track {
            margin-left: 0px !important;
        }
        
    </style>

    @if (setting('site.custom_css') !== null)
        <style>
            {!!setting('site.custom_css')!!}    
        </style>
    @endif

    @yield('style')

</head>

<body>
    <div class="container-fluid">
        <header>
            @yield('header')
        <header>
    </div>
        @yield('content')
   

    <script src="{{asset("/js/app.js")}}?{{time()}}"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>


    <script src="https://kit.fontawesome.com/55959cb7f8.js" crossorigin="anonymous"></script>
    <script src="https://player.vimeo.com/api/player.js"></script>

    <script>
        $('.slider-mobile').slick({
            infinite: false,
            slidesToShow: 2.5,
            slidesToScroll: 1,
            arrows: false,
        });

        $('.slider-historico').slick({
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 550,
                    settings: {
                        slidesToShow: 1
                    }
                },
            ]
        });

        $('.slider-equipo').slick({
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 550,
                    settings: {
                        slidesToShow: 1
                    }
                },
            ]
        });
    </script>

    <script>
        checkBackNav();
        
        function checkBackNav(){
            if(window.location.pathname == "/" || window.location.pathname == "/preview"){
                document.getElementById('back-nav').style.display = 'none';
            }         
        }

        function showOverlayCompartir(displayType){
            document.getElementById('overlayCompartir').style.display = displayType;
            document.getElementById('overlayCompartirCerrar').style.display = displayType;
            if(displayType == 'block'){
                document.getElementById('overlayCompartirBotones').style.display = 'flex';
            }else{
                document.getElementById('overlayCompartirBotones').style.display = displayType;
            }
        }

        function showOverlayTakeHome(displayType){
            document.getElementById('overlayCompartirTakeHome').style.display = displayType;
            document.getElementById('overlayCompartirTakeHomeCerrar').style.display = displayType;
            if(displayType == 'block'){
                document.getElementById('overlayCompartirTakeHomeBotones').style.display = 'flex';
            }else{
                document.getElementById('overlayCompartirTakeHomeBotones').style.display = displayType;
            }
        }
    </script>
    
    <script>
        function showButtons(id){
            var elem = document.getElementById(id);
            var displayValue = elem.style.display;
            var botones = document.getElementsByClassName('video-share-button-4');
            
            if(displayValue == 'block'){
                displayValue = 'none';
            }else{
                displayValue = 'block';
            }
            
            Array.prototype.forEach.call(botones, function(element) {
                element.style.display = 'none'; 
                element.style.opacity = 0;                             
            });
            
            elem.style.display = displayValue;
            
            var pos = 50;
            var opa = 0
            var id = setInterval(function() {
                if (pos == 0) {
                    clearInterval(id);
                } else {
                    pos--;
                    opa += 0.05
                    elem.style.top = pos + 'px';
                elem.style.opacity = opa;
            }
        }, 10);
    }
    </script>
</body>
</html>