<div class="container-fluid">
    <div id="conocenos-web">
        <h3 class="titulo-categoria mt-5 mb-3"><strong>CONOCE A NUESTRO EQUIPO</strong></h3>
        <div class="row mb-5 mx-1 pb-5 slider-equipo">
            @foreach ($equipo as $miembro)                
                <div class="col px-0 mx-1">
                    <figure class="mb-0">
                        <img src="/storage/{{$miembro->participante->imagen}}" class="img-fluid" />
                        <div class="equipo-overlay ">
                            <span class="text-uppercase equipo-tipo-trabajador font-weight-bold">
                                @if ($miembro->tipo == 'Coordinador')                                
                                <i class="fas fa-2x fa-user-md"></i>
                                &nbsp;{{$miembro->tipo}}</span>                              
                                @else
                                <i class="fas fa-2x fa-chalkboard-teacher"></i>
                                &nbsp;{{$miembro->tipo}}</span>
                                @endif
                        </div>
                        <div class="equipo-overlay-footer d-flex flex-column justify-content-center px-2">
                            <span class="font-weight-bold">{{$miembro->participante->nombre_completo}}</span>
                            <span class="font-italic font-weight-light">{!!$miembro->participante->filiacion!!}</span>
                        </div>
                    </figure>
                </div>            
            @endforeach
        </div>
    </div>
</div>

<style>
    .slider-equipo .slick-arrow:focus {
        outline: none;
    }

    .slider-equipo .slick-arrow {
        position: absolute;
        height: 3em;
        width: 3em;
        top: 50%;
        transform: translateY(-100%);
        color: #fff;
        background-color: rgba({{colorHexToRGB(setting('site.color_primario'))}}, 0.9);
        border: none;
        z-index: 1;
    }

    .slider-equipo .slick-next {
        right: 2px;
    }

    .slider-equipo .slick-prev {
        left: 4px;
    }

    /* .slider-equipo .slick-slide:not(.slick-active) {
        margin-right: 5px !important;
    }

    .slider-equipo .slick-current {
        margin-right: 5px !important;
    }

    .slider-equipo .slick-active:not(.slick-current) {
        margin-left: 5px !important;
    } */

    @media (max-width: 550px) {
        .slider-equipo .slick-next {
            right: 5px;
        }
    }
</style>