<style>

    .js-cookie-consent {
        position: fixed;
        bottom: 0;
        left: 0;
        right: 0;
        z-index: 9999;
        opacity: .8;
        min-height: 40px;
        font-size: 1vw;
        padding: .6vw 0 .6vw 2.2vw;
        width: 100%;
    }

    .js-cookie-consent-agree {
        padding: 1vw 1.5vw;
        min-height: 2.5vw;
        cursor: pointer;
        border-radius: 30px;
        border: none;
        background-color: {{setting('site.color_primario')}};
    }
    .js-cookie-consent-agree:hover{
        background-color: rgb({{colorHexToRGB(setting('site.color_primario'))}}, 0.7);
    }

    @media (max-width: 767.98px) {
        .js-cookie-consent {
            font-size: 2vw;
        }

        .js-cookie-consent-agree {
            padding: 1vw 5vw;
        }
    }

    @media (max-width: 1023.98px) and (min-width: 768px) {
        .js-cookie-consent {
            font-size: 1.5vw;
        }

        .js-cookie-consent-agree {
            padding: 1vw 3vw;
        }
    }

    @media (min-width: 1440px) {
        .js-cookie-consent {
            font-size: 14.4px;
            padding: 8.65px 0 8.65px 31.7px;
        }

        .js-cookie-consent-agree {
            padding: 14.4px 43.2px;
            min-height: 36px;
        }
    }


</style>


<div class="js-cookie-consent cookie-consent fondo-color-secundario">
    <div class="row justify-content-center">
        <div class="col-8">
            <span class="cookie-consent__message text-white">
                {!! trans('cookieConsent::texts.message') !!}
            </span>
            <a href="{{ route('politica-cookies') }}">Política de Cookies</a>
        </div>
        <div class="col-2 d-flex align-items-center">
            <button class="js-cookie-consent-agree cookie-consent__agree text-white">
                {{ trans('cookieConsent::texts.agree') }}
            </button>
        </div>
    </div>
</div>
