<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    // OJO: SI HAS CREADO O MODIFICADO UN BREAD, MENU, PERMISOS, ROLES O
    // AÑADIDO UN CAMPO A SETTINGS DEBES INSERTAR LOS CAMBIOS EN LOS 
    // SEEDERS QUE CORRESPONDAN. SI NO, AL EJECUTAR LOS SEEDERS SE PERDERÁN
    // LOS CAMBIOS
    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // BREADs
        \DB::table('data_rows')->delete();
        \DB::table('data_types')->delete();
        $this->call(DataTypesTableSeeder::class);
        $this->call(DataRowsTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);

        // Usuarios, roles y permisos
        \DB::table('permission_role')->delete();
        \DB::table('users')->delete();
        \DB::table('roles')->delete();
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);

        //Menús y Settings
        $this->call(MenusTableSeeder::class);
        $this->call(MenuItemsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
    
        // Contenido Demo (Eventos, categorias, participantes, videos, videos_ponentes)
        // $this->call(EventosTableSeeder::class);
        // $this->call(CategoriasTableSeeder::class);
        // $this->call(ParticipantesTableSeeder::class);
        // $this->call(VideosTableSeeder::class);
        // $this->call(VideosPonentesTableSeeder::class);

        // Contenido Demo (Voyager)
        // $this->call(categoriesTableSeeder::class);
        // $this->call(PostsTableSeeder::class);
        // $this->call(PagesTableSeeder::class);

    }
}
