<?php

use Illuminate\Database\Seeder;

class VideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('videos')->delete();
        
        \DB::table('videos')->insert( array(
            array('id' => '1','titulo' => 'Video 1','imagen' => 'videos\\January2020\\APjaLKbpl3Q5HiK3VtOA.png','categoria_id' => '1','enlace' => 'https://player.vimeo.com/video/341047756','fecha' => '2020-02-03 16:33:00','deleted_at' => NULL,'created_at' => '2020-01-28 16:16:44','updated_at' => '2020-02-03 15:33:31'),
            array('id' => '2','titulo' => 'Video 2','imagen' => 'videos\\January2020\\jxN505hG3wzXHw512aoZ.png','categoria_id' => '2','enlace' => 'https://player.vimeo.com/video/341165109','fecha' => '2020-02-03 17:18:00','deleted_at' => NULL,'created_at' => '2020-01-28 16:21:52','updated_at' => '2020-02-03 16:18:27'),
            array('id' => '3','titulo' => 'Video 3','imagen' => 'videos\\January2020\\cK3H91yG10kYt6cSO6Pa.png','categoria_id' => '2','enlace' => 'https://player.vimeo.com/video/341501625','fecha' => '2020-05-20 17:18:00','deleted_at' => NULL,'created_at' => '2020-01-28 16:22:06','updated_at' => '2020-02-03 16:18:51'),
            array('id' => '4','titulo' => 'Video 4','imagen' => 'videos\\February2020\\RjEGXlyeJLBXifoZ3POp.png','categoria_id' => '2','enlace' => 'https://player.vimeo.com/video/341165109','fecha' => '2020-02-21 17:25:00','deleted_at' => NULL,'created_at' => '2020-02-19 16:26:05','updated_at' => '2020-02-19 16:26:05'),
            array('id' => '5','titulo' => 'Video 5','imagen' => 'videos\\February2020\\30fNsxZU9eMv7wcJix5y.png','categoria_id' => '2','enlace' => 'https://player.vimeo.com/video/379307511','fecha' => '2020-02-10 17:26:00','deleted_at' => NULL,'created_at' => '2020-02-19 16:26:41','updated_at' => '2020-02-19 16:26:41'),
            array('id' => '6','titulo' => 'Video 6','imagen' => 'videos\\February2020\\sDby9JQzCO5zIIGBzZwu.png','categoria_id' => '2','enlace' => 'https://player.vimeo.com/video/379307511','fecha' => '2020-02-03 17:29:00','deleted_at' => NULL,'created_at' => '2020-02-19 16:29:07','updated_at' => '2020-02-19 16:29:07')
        ));
    }    
}
