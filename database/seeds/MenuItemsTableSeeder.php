<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class MenuItemsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('menu_items')->delete();
        \DB::table('menu_items')->insert( array(
            array('id' => '1','menu_id' => '1','title' => 'Dashboard','url' => '','target' => '_self','icon_class' => 'voyager-boat','color' => NULL,'parent_id' => '5','order' => '1','created_at' => '2020-01-24 13:21:29','updated_at' => '2020-01-28 13:27:52','route' => 'voyager.dashboard','parameters' => NULL),
            array('id' => '2','menu_id' => '1','title' => 'Media','url' => '','target' => '_self','icon_class' => 'voyager-images','color' => NULL,'parent_id' => '5','order' => '3','created_at' => '2020-01-24 13:21:29','updated_at' => '2020-01-28 13:30:53','route' => 'voyager.media.index','parameters' => NULL),
            array('id' => '3','menu_id' => '1','title' => 'Users','url' => '','target' => '_self','icon_class' => 'voyager-person','color' => NULL,'parent_id' => '5','order' => '6','created_at' => '2020-01-24 13:21:29','updated_at' => '2020-01-29 13:36:07','route' => 'voyager.users.index','parameters' => NULL),
            array('id' => '4','menu_id' => '1','title' => 'Roles','url' => '','target' => '_self','icon_class' => 'voyager-lock','color' => NULL,'parent_id' => '5','order' => '5','created_at' => '2020-01-24 13:21:29','updated_at' => '2020-01-29 10:27:39','route' => 'voyager.roles.index','parameters' => NULL),
            array('id' => '5','menu_id' => '1','title' => 'Tools','url' => '','target' => '_self','icon_class' => 'voyager-tools','color' => NULL,'parent_id' => NULL,'order' => '8','created_at' => '2020-01-24 13:21:29','updated_at' => '2020-03-02 17:42:15','route' => NULL,'parameters' => NULL),
            array('id' => '6','menu_id' => '1','title' => 'Menu Builder','url' => '','target' => '_self','icon_class' => 'voyager-list','color' => NULL,'parent_id' => '5','order' => '4','created_at' => '2020-01-24 13:21:29','updated_at' => '2020-01-28 13:30:53','route' => 'voyager.menus.index','parameters' => NULL),
            array('id' => '7','menu_id' => '1','title' => 'Database/BREAD','url' => '','target' => '_self','icon_class' => 'voyager-data','color' => '#000000','parent_id' => '5','order' => '2','created_at' => '2020-01-24 13:21:29','updated_at' => '2020-01-28 13:27:52','route' => 'voyager.database.index','parameters' => 'null'),
            array('id' => '8','menu_id' => '1','title' => 'Compass','url' => '','target' => '_self','icon_class' => 'voyager-compass','color' => NULL,'parent_id' => '5','order' => '7','created_at' => '2020-01-24 13:21:29','updated_at' => '2020-03-02 17:42:15','route' => 'voyager.compass.index','parameters' => NULL),
            array('id' => '10','menu_id' => '1','title' => 'Settings','url' => '','target' => '_self','icon_class' => 'voyager-settings','color' => NULL,'parent_id' => NULL,'order' => '9','created_at' => '2020-01-24 13:21:29','updated_at' => '2020-01-29 13:36:08','route' => 'voyager.settings.index','parameters' => NULL),
            array('id' => '11','menu_id' => '1','title' => 'Categories','url' => '','target' => '_self','icon_class' => 'voyager-categories','color' => NULL,'parent_id' => '5','order' => '10','created_at' => '2020-01-24 13:21:29','updated_at' => '2020-03-02 17:42:15','route' => 'voyager.categories.index','parameters' => NULL),
            array('id' => '12','menu_id' => '1','title' => 'Posts','url' => '','target' => '_self','icon_class' => 'voyager-news','color' => NULL,'parent_id' => '5','order' => '9','created_at' => '2020-01-24 13:21:29','updated_at' => '2020-03-02 17:42:15','route' => 'voyager.posts.index','parameters' => NULL),
            array('id' => '13','menu_id' => '1','title' => 'Pages','url' => '','target' => '_self','icon_class' => 'voyager-file-text','color' => NULL,'parent_id' => '5','order' => '11','created_at' => '2020-01-24 13:21:29','updated_at' => '2020-01-29 13:36:07','route' => 'voyager.pages.index','parameters' => NULL),
            array('id' => '14','menu_id' => '1','title' => 'Hooks','url' => '','target' => '_self','icon_class' => 'voyager-hook','color' => NULL,'parent_id' => '5','order' => '8','created_at' => '2020-01-24 13:21:29','updated_at' => '2020-03-02 17:42:15','route' => 'voyager.hooks','parameters' => NULL),
            array('id' => '15','menu_id' => '1','title' => 'Web','url' => '/','target' => '_blank','icon_class' => 'voyager-home','color' => '#000000','parent_id' => NULL,'order' => '1','created_at' => '2020-01-28 11:27:19','updated_at' => '2020-01-28 11:27:32','route' => NULL,'parameters' => ''),
            array('id' => '16','menu_id' => '1','title' => 'Eventos','url' => '','target' => '_self','icon_class' => 'voyager-trophy','color' => '#000000','parent_id' => NULL,'order' => '3','created_at' => '2020-01-28 13:19:20','updated_at' => '2020-01-28 16:48:24','route' => 'voyager.eventos.index','parameters' => 'null'),
            array('id' => '17','menu_id' => '1','title' => 'Categorias','url' => '','target' => '_self','icon_class' => 'voyager-categories','color' => '#000000','parent_id' => NULL,'order' => '2','created_at' => '2020-01-28 13:23:37','updated_at' => '2020-01-28 16:37:33','route' => 'voyager.categorias.index','parameters' => 'null'),
            array('id' => '18','menu_id' => '1','title' => 'Videos','url' => '','target' => '_self','icon_class' => 'voyager-youtube-play','color' => '#000000','parent_id' => NULL,'order' => '4','created_at' => '2020-01-28 16:14:57','updated_at' => '2020-01-28 16:47:12','route' => 'voyager.videos.index','parameters' => 'null'),
            array('id' => '19','menu_id' => '1','title' => 'Participantes','url' => '','target' => '_self','icon_class' => 'voyager-people','color' => '#000000','parent_id' => NULL,'order' => '5','created_at' => '2020-01-29 10:21:07','updated_at' => '2020-01-29 10:22:19','route' => 'voyager.participantes.index','parameters' => 'null'),
            array('id' => '20','menu_id' => '1','title' => 'Colaboradores','url' => '','target' => '_self','icon_class' => 'voyager-certificate','color' => '#000000','parent_id' => NULL,'order' => '6','created_at' => '2020-01-29 13:01:19','updated_at' => '2020-01-29 13:37:03','route' => 'voyager.colaboradores.index','parameters' => 'null'),
            array('id' => '21','menu_id' => '1','title' => 'Eventos Equipos','url' => '','target' => '_self','icon_class' => 'voyager-group','color' => '#000000','parent_id' => NULL,'order' => '7','created_at' => '2020-03-02 17:25:45','updated_at' => '2020-03-02 17:42:30','route' => 'voyager.eventos-equipos.index','parameters' => 'null')
        ));
    }
}
