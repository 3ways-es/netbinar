<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        
        \DB::table('categorias')->delete();
        
        \DB::table('categorias')->insert( array(
            array('id' => '1','nombre' => 'Categoría Destacado','order' => '1','mostrar_titulo' => '1','evento_id' => '1','destacado' => '1','deleted_at' => NULL,'created_at' => '2020-02-07 12:54:56','updated_at' => NULL),
            array('id' => '2','nombre' => 'Categoría columnas','order' => '2','mostrar_titulo' => '1','evento_id' => '1','destacado' => '0','deleted_at' => NULL,'created_at' => '2020-02-07 12:54:56','updated_at' => NULL)    
        ));
    }

}
