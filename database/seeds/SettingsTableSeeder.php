<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        \DB::table('settings')->delete();
        \DB::table('settings')->insert( array(
            array('id' => '1','key' => 'site.title','display_name' => 'Site Title','value' => 'CongressTV','details' => '','type' => 'text','order' => '4','group' => 'Site'),
            array('id' => '2','key' => 'site.description','display_name' => 'Site Description','value' => 'Site Description','details' => '','type' => 'text','order' => '6','group' => 'Site'),
            array('id' => '3','key' => 'site.logo','display_name' => 'Site Logo','value' => 'settings\\January2020\\hn6Gr8lEnR9i7ycoOvng.png','details' => '','type' => 'image','order' => '7','group' => 'Site'),
            array('id' => '4','key' => 'site.google_analytics_tracking_id','display_name' => 'Google Analytics Tracking ID','value' => NULL,'details' => '','type' => 'text','order' => '15','group' => 'Site'),
            array('id' => '5','key' => 'admin.bg_image','display_name' => 'Admin Background Image','value' => '','details' => '','type' => 'image','order' => '5','group' => 'Admin'),
            array('id' => '6','key' => 'admin.title','display_name' => 'Admin Title','value' => 'Voyager','details' => '','type' => 'text','order' => '1','group' => 'Admin'),
            array('id' => '7','key' => 'admin.description','display_name' => 'Admin Description','value' => 'Welcome to Voyager. The Missing Admin for Laravel','details' => '','type' => 'text','order' => '2','group' => 'Admin'),
            array('id' => '8','key' => 'admin.loader','display_name' => 'Admin Loader','value' => '','details' => '','type' => 'image','order' => '3','group' => 'Admin'),
            array('id' => '9','key' => 'admin.icon_image','display_name' => 'Admin Icon Image','value' => '','details' => '','type' => 'image','order' => '4','group' => 'Admin'),
            array('id' => '10','key' => 'admin.google_analytics_client_id','display_name' => 'Google Analytics Client ID (used for admin dashboard)','value' => NULL,'details' => '','type' => 'text','order' => '1','group' => 'Admin'),
            array('id' => '11','key' => 'site.color_primario','display_name' => 'Color Primario','value' => '#C13262','details' => NULL,'type' => 'text','order' => '8','group' => 'Site'),
            array('id' => '12','key' => 'site.color_secundario','display_name' => 'Color Secundario','value' => '#000000','details' => NULL,'type' => 'text','order' => '9','group' => 'Site'),
            array('id' => '13','key' => 'site.web_principal','display_name' => 'Web Principal','value' => 'http://gesida-seimc.org/','details' => NULL,'type' => 'text','order' => '10','group' => 'Site'),
            array('id' => '14','key' => 'site.facebook','display_name' => 'Enlace Facebook','value' => 'http://www.facebook.com/Gesida/','details' => NULL,'type' => 'text','order' => '12','group' => 'Site'),
            array('id' => '15','key' => 'site.twitter','display_name' => 'Enlace Twitter','value' => 'http://www.twitter.com/Gesida/','details' => NULL,'type' => 'text','order' => '13','group' => 'Site'),
            array('id' => '16','key' => 'site.nombre_web_principal','display_name' => 'Nombre Web Principal','value' => NULL,'details' => NULL,'type' => 'text','order' => '11','group' => 'Site'),
            array('id' => '18','key' => 'site.linkedin','display_name' => 'Enlace LinkedIn','value' => NULL,'details' => NULL,'type' => 'text','order' => '14','group' => 'Site'),
            array('id' => '19','key' => 'site.modo_mantenimiento','display_name' => 'Modo mantenimiento','value' => '1','details' => NULL,'type' => 'checkbox','order' => '2','group' => 'Site'),
            array('id' => '21','key' => 'site.evento_actual','display_name' => 'Evento actual','value' => '1','details' => '{"model":"App\\\\Evento","value":"id","label":"titulo"}','type' => 'select_dropdown','order' => '3','group' => 'Site'),
            array('id' => '22','key' => 'site.imagen_fondo','display_name' => 'imagen de Fondo','value' => '','details' => NULL,'type' => 'image','order' => '16','group' => 'Site'),
            array('id' => '23','key' => 'site.font_links','display_name' => 'Font Links','value' => NULL,'details' => NULL,'type' => 'code_editor','order' => '19','group' => 'Site'),
            array('id' => '24','key' => 'site.custom_css','display_name' => 'Custom Css','value' => NULL,'details' => NULL,'type' => 'code_editor','order' => '20','group' => 'Site')
            ));
    }
}
