<?php

use Illuminate\Database\Seeder;

class ParticipantesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('participantes')->delete();
        
        \DB::table('participantes')->insert( array(
            array('id' => '1','nombre' => 'Doña','apellidos' => 'Gumersinda','filiacion' => NULL,'imagen' => 'http://somos3ways.com/img/team/neli.jpg','created_at' => '2020-01-29 10:33:00','updated_at' => '2020-01-29 10:33:35','deleted_at' => NULL,'tratamiento' => 'Dra.'),
            array('id' => '2','nombre' => 'Ricardo','apellidos' => 'Morese','filiacion' => NULL,'imagen' => 'http://somos3ways.com/img/team/ricardo.jpg','created_at' => '2020-01-29 11:49:22','updated_at' => '2020-01-29 11:49:22','deleted_at' => NULL,'tratamiento' => 'Dr.')
        ));
    }
}
