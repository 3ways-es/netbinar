<?php

use Illuminate\Database\Seeder;

class EventosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('eventos')->delete();
        
        \DB::table('eventos')->insert( array(
            array('id' => '1','titulo' => 'Primer evento','created_at' => '2020-01-28 13:31:19','updated_at' => '2020-01-28 13:31:19','deleted_at' => NULL,'enlace' => NULL)
        ));
    }
}
