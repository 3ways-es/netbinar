<?php

use Illuminate\Database\Seeder;

class VideosPonentesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('videos_ponentes')->delete();
        
        \DB::table('videos_ponentes')->insert( array(
            array('id' => '1','video_id' => '3','participante_id' => '1','created_at' => NULL,'updated_at' => NULL),
            array('id' => '2','video_id' => '3','participante_id' => '2','created_at' => NULL,'updated_at' => NULL),
            array('id' => '3','video_id' => '4','participante_id' => '2','created_at' => NULL,'updated_at' => NULL),
            array('id' => '4','video_id' => '5','participante_id' => '1','created_at' => NULL,'updated_at' => NULL),
            array('id' => '5','video_id' => '6','participante_id' => '1','created_at' => NULL,'updated_at' => NULL)
        ));
    }
}
