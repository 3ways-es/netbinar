<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameTablePonentesToParticipantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('ponentes', 'participantes');

        Schema::table('videos_ponentes', function (Blueprint $table) {
            $table->renameColumn('ponente_id', 'participante_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('participantes', 'ponentes');

        Schema::table('videos_ponentes', function (Blueprint $table) {
            $table->renameColumn('participante_id', 'ponente_id');
        });

    }
}
