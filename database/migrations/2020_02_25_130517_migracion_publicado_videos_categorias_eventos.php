<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MigracionPublicadoVideosCategoriasEventos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->tinyinteger('publicado')->nullable(false)->default(0);
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->tinyinteger('publicado')->nullable(false)->default(0);
        });

        Schema::table('eventos', function (Blueprint $table) {
            $table->tinyinteger('publicado')->nullable(false)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->dropColumn('publicado');
        });

        Schema::table('categorias', function (Blueprint $table) {
            $table->dropColumn('publicado');
        });

        Schema::table('eventos', function (Blueprint $table) {
            $table->dropColumn('publicado');
        });
    }
}
