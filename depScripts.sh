#!/bin/sh
# activate maintenance mode

now=$(date +"Deploy_%Y-%m-%d_%H:%M:%S")

echo "*************** Inicia scripts deploy *************"  >> log/$now.log

php artisan down 					    				    >> log/$now.log
php artisan cache:clear                                     >> log/$now.log
composer install --no-interaction --no-dev --prefer-dist	>> log/$now.log
php artisan migrate --force				    				>> log/$now.log
php artisan clear-compiled								    >> log/$now.log
php artisan optimize									    >> log/$now.log
php artisan db:seed --force				    			    >> log/$now.log
php artisan config:clear 				    				>> log/$now.log
php artisan route:clear					    				>> log/$now.log
php artisan config:cache	 			            		>> log/$now.log
php artisan route:cache 				    				>> log/$now.log
php artisan up					    					    >> log/$now.log

echo "*************** Fin scripts deploy ***************"    >> log/$now.log
