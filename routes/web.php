<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/{evento?}", 'HomeController@index')->name('home')->where('evento', '[0-9]+');

Route::get("videos/{id}", 'VideoController@index')->name('video');

Route::get("aviso-legal", function() {
    return view('aviso-legal');
})->name('aviso-legal');

Route::get("politica-de-privacidad", function() {
    return view('politica-privacidad');
})->name('politica-privacidad');

Route::get("politica-de-cookies", function() {
    return view('politica-cookies');
})->name('politica-cookies');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

//Redirigir al login de Voyager si se intenta acceder a un contenido solo accesible a usuarios
Route::redirect('login', 'admin')->name('login');

Route::group(['middleware' => 'auth'], function () {
    Route::get("/preview/{evento?}", 'HomeController@preview')->name('preview')->where('evento', '[0-9]+');
});
